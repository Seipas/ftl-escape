<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$fleets = $entityManager->getRepository('Fleet')->findAll();

foreach ($fleets as $fleet)
{
    $politicalsystem = $fleet->getPoliticalSystem();
    if (!is_null($politicalsystem) && !$politicalsystem->getElection() && is_null($fleet->getChief()))
    {
		//$player = $fleet->getPlayer();
		$characters = $fleet->getCharacters();
		foreach ($characters as $character)
		{
			if (is_null($fleet->getChief()) && $character->getPoliticalSystem()->getId() === $politicalsystem->getId())
			{
				$fleet->setChief($character);
				$message = new Message(null,$fleet->getPlayer(),'msg.character.take.power',true,array($character->getName()));
				$entityManager->persist($message);
			}
		}
		if (is_null($fleet->getChief()))
		{
			$fleet->setPoliticalSystem(null);
			$message = new Message(null,$fleet->getPlayer(),'msg.no.political.system.set',true);
			$entityManager->persist($message);
		}
    }
}
$entityManager->flush();
