<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$ships = $fleet->getShips();
	$politicalsystem = $fleet->getPoliticalSystem();
	$moralproduction=0;
	foreach ($ships as $ship)
	{
		$moralproduction += $ship->getType()->getMoralProduction();
	}
	$bonus=1;
	if (!is_null($politicalsystem))
	{
		$bonus = 1 + $politicalsystem->getMoralBonus();
	}
	$characters = $fleet->getCharacters();
	foreach ($characters as $character)
	{
		$bonus += $character->getMoralBonus();
	}
	
	$moralproduction = $moralproduction * $bonus;
	$fleet->increaseMoral($moralproduction);
}

$entityManager->flush();
