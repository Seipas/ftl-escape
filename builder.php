<?php

require_once(__DIR__.'/bootstrap.php');
require_once __DIR__.'/const.php';
require_once(__DIR__.'/lib/i18n.php');
require_once(__DIR__.'/tools.php');
require_once(__DIR__.'/helper.php');

class Builder {
	static function buildFleet($player)
	{
		global $entityManager;
		$fleet = new Fleet($player);
		$fleet->setName('Default Fleet Name');

		Builder::populateFleet($fleet,true);

		return $fleet;
	}

	static function reinitFleetStocks($fleet)
	{
		$fleet->setFood(INIT_FOOD);
		$fleet->setFuel(INIT_FUEL);
		$fleet->setMaterial(INIT_MATERIAL);
		$fleet->setMedicine(INIT_MEDICINE);
		$fleet->setMoral(INIT_MORAL);
		$fleet->setQualifiedStaff(0);
	}
	
	static function broadcastMessage($messagestring,$infos=array())
	{
		global $entityManager;
		$players = $entityManager->getRepository('Player')->findAll();
		foreach ($players as $player)
		{
			$message = new Message(null,$player,$messagestring,true,$infos);
			$entityManager->persist($message);
		}
		$entityManager->flush();
	}

	static function populateFleet(&$fleet,$first=false,$nbShip=null)
	{
		global $entityManager;
		if ($first)
		{
			$battlecruiserType = $entityManager->getRepository('ShipType')->find(BATTLECRUISER_ID);

			$battlecruiser = new Ship($fleet,$battlecruiserType);
			$battlecruiser->setName('Default Battlecruiser Name');
			$battlecruiser->setHP($battlecruiserType->getMaxHP());
			$battlecruiser->setPassengers($battlecruiserType->getMaxPassengers());
			$battlecruiser->setStaff($battlecruiserType->getQualifiedStaff());
			$entityManager->persist($battlecruiser);
			$fleet->setAdmiralShip($battlecruiser);
		}

		if (is_null($nbShip))
		{
			$nbShip = rand(MIN_START_NB_SHIP,MAX_START_NB_SHIP);
		}
		// search available ships
		$qb = $entityManager->createQueryBuilder();
		$qb->select('s')
			->from('ShipType','s')
			->where('s.buildable = ?1')
			->andWhere('s.id != 1')
			->setParameter(1,true);
		$availableShips = $qb->getQuery()->getResult();
		$nbAvailableShips = count($availableShips);

		$canMine=false;
		$canScout=false;
		$canRepair=false;
		$canProduce=false;

		for ($num=0;$num<$nbShip;$num++)
		{
			$randType = $availableShips[rand(0,$nbAvailableShips-1)];
			$canMine = $canMine || $randType->canMine();
			$canScout = $canScout || $randType->canScout();
			$canRepair = $canRepair || $randType->canRepair();
			$canProduce = $canProduce || $randType->canProduceShips();
			Builder::buildShip($fleet,$randType,rand(0,$randType->getMaxPassengers()),$randType->getQualifiedStaff());
		}
		if ($first)
		{
			$fleet->setMaterial(INIT_MATERIAL);
			$fleet->setFood(INIT_FOOD);
			$fleet->setFuel(INIT_FUEL);
			$fleet->setMoral(INIT_MORAL);
			$fleet->setMedicine(INIT_MEDICINE);
			if (!$canMine)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('mine'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canScout)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('scout'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canRepair)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('repair'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canProduce)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('produceShips'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
		}
	}

	static function buildRandomCharacter($fleet)
	{
		global $entityManager;
		$types = array('military','civilian');
		$typeId = rand(0,1);
		$type = $types[$typeId];

		$qb = $entityManager->createQueryBuilder();
		$qb->select('ps')
			->from('PoliticalSystem','ps');
		$systems = $qb->getQuery()->getResult();
		$nbAvailablePS = count($systems);
		$randPS = $systems[rand(0,$nbAvailablePS-1)];

		$avatar = rand(1,CHARACTER_NB_AVATARS);

		$gender = rand(0,99);
		$lines = file(__DIR__.'/'.DICO_REP.'firstnames.txt');
		$firstname = $lines[array_rand($lines)];
		$lines = file(__DIR__.'/'.DICO_REP.'lastnames.txt');
		$lastname = $lines[array_rand($lines)];
		$name = $firstname.' '.$lastname;


		$ships = $fleet->getShips();
		$keys = array_keys($ships);
		$nbships = count($ships);
		$keyId = rand(1,$nbships) - 1;
		$ship = $ships[$keys[$keyId]];

		return Builder::buildCharacter($fleet, $ship, $name, $gender, $type,$randPS,$avatar);
	}

	static function buildCharacter($fleet, $ship, $name, $gender, $type, $politicalSystem, $avatar=null)
	{
		global $entityManager;
		$moral = rand(CHARACTER_MIN_MORAL_BONUS,CHARACTER_MAX_MORAL_BONUS) / 100;
		$popularity = rand(CHARACTER_MIN_POPULARITY,CHARACTER_MAX_POPULARITY);
		$character = new Character($fleet,$ship,$name,$type);
		$character->setAvatar($avatar);

		$attack = $defense = 0;
		if ($type === 'military')
		{
			$attack = rand(CHARACTER_MIN_ATTACK_BONUS,CHARACTER_MAX_ATTACK_BONUS) / 100;
			$defense = rand(CHARACTER_MIN_DEFENSE_BONUS,CHARACTER_MAX_DEFENSE_BONUS) / 100;
		}

		$character->setAttackBonus($attack);
		$character->setDefenseBonus($defense);
		$character->setMoralBonus($moral);
		$character->setType($type);
		$character->setGender($gender);
		$character->setPoliticalSystem($politicalSystem);
		$character->setPopularity($popularity);
		return $character;
	}

	static function buildShip($fleet,$type,$passengers=0,$staff=0,$hp=null,$returnShip=false)
	{
		global $entityManager;
		if ($type->isUnique())
		{
			$qb = $entityManager->createQueryBuilder();
			$qb->select('s')
				->from('Ship','s')
				->where('s.type = :type')
				->andWhere('s.fleet = :fleet')
				->setParameter('type',$type)
				->setParameter('fleet',$fleet);
			if (count($qb->getQuery()->getResult()) >= 1)
			{
				return false;
			}
		}
		$newShip = new Ship($fleet,$type);
		$lines = file(__DIR__.'/'.DICO_REP.$type->getNameDico().'.txt');
		$name = $lines[array_rand($lines)];
		$newShip->setName($name);
		if (is_null($hp))
		{
			$newShip->setHP($type->getMaxHP());
		}
		else
		{
			$newShip->setHP($hp);
		}
		$newShip->setPassengers($passengers);
		$newShip->setStaff($staff);
		$entityManager->persist($newShip);
		if ($returnShip)
		{
			return $newShip;
		}
		else
		{
			return true;
		}
	}

	static function buildSector($player,$firstsector=false,$withennemies=true)
	{
		global $entityManager;
		$i18n = new I18n();
		$i18n->autoSetLang();
		$sector = new Sector($player);
		$dice = ($firstsector) ? 0 : rand(1,100);

		$hasEnnemies = $withennemies && ($firstsector || $dice <= 20);
		if ($hasEnnemies)
		{
			$nbEnnemies=0;
			if ($firstsector)
			{
				$nbEnnemies = rand(1,MAX_START_NB_ENNEMIES);
			}
			else
			{
				$nbEnnemies = rand(1,MAX_ENNEMIES_PER_SECTOR);
			}
			Builder::populateSector($sector,$nbEnnemies,$firstsector);
			if (!$firstsector)
			{
				Tools::setFlashMsg($i18n->getText('msg.ennemy.detected'));
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.no.ennemy.detected'));
		}

		if (!$firstsector)
		{
			$material = rand(SECTOR_MIN_MATERIAL,SECTOR_MAX_MATERIAL);
			$uranium = rand(0,MAX_URANIUM_PER_SECTOR);
			$sector->setMaterial($material);
			$sector->setUranium($uranium);
		}

		// generate wrecks
		$dice = rand(1,100);

		if ($dice <= CHANCE_OF_WRECK_IN_SECTOR)
		{
			$nbwreck = rand(1,MAX_WRECKS_PER_SECTOR);
			$sector->addWrecks($nbwreck);
		}
		return $sector;
	}

	static function populateSector(&$sector,$nbEnnemies,$firstsector=false)
	{
		global $entityManager;
		$player = $sector->getPlayer();
		// hugly hack : does not work if you delete a ShipType
		// @TODO : find a better way to randomize ship types
		$nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());

		for ($num=0;$num<$nbEnnemies;$num++)
		{
			$randType = null;
			$difficulty = MAX_DIFFICULTY + 1;
			$maxdifficulty = MAX_DIFFICULTY;

			if ($firstsector)
			{
				$difficulty = MAX_START_DIFFICULTY + 1;
				$maxdifficulty = MAX_START_DIFFICULTY;
			}
			else
			{
				$maxdifficulty = Helper::getDifficulty($player);
			}

			while ($difficulty > $maxdifficulty)
			{
				$randType = $entityManager->getRepository('EnnemyShipType')->find(rand(1,$nbAvailableShips));
				$difficulty = $randType->getDifficulty();
			}

			$ennemyShip = new EnnemyShip($sector,$randType,'Bandit '.$num);
			$ennemyShip->setHP($randType->getMaxHP());
			$entityManager->persist($ennemyShip);
		}
	}

	static function buildPlanetSector($player,$planet)
	{
		global $entityManager;
		$i18n = new I18n();
		$i18n->autoSetLang();
		$sector = new Sector($player,$planet);
		$garrison = $planet->getGarrison();
		$cpt = 0;
		//$enemyids = array_keys($garrison);
		foreach ($garrison as $id=>$nb)
		{
			$type = $entityManager->getRepository('EnnemyShipType')->find($id);
			for ($i=0; $i < $nb; $i++)
			{
				$enemyShip = new EnnemyShip($sector,$type,'Bandit '.$cpt);
				$enemyShip->setHP($type->getMaxHP());
				$entityManager->persist($enemyShip);
				$cpt++;
			}
		}
		$entityManager->persist($sector);
	}
	
	static function buildNexusSector($player,$nexus)
	{
		global $entityManager;
		$i18n = new I18n();
		$i18n->autoSetLang();
		$sector = new Sector($player,null,$nexus);
		$garrison = $nexus->getGarrison();
		$cpt = 0;
		foreach ($garrison as $id=>$nb)
		{
			$type = $entityManager->getRepository('EnnemyShipType')->find($id);
			for ($i=0; $i < $nb; $i++)
			{
				$enemyShip = new EnnemyShip($sector,$type,'Bandit '.$cpt);
				$enemyShip->setHP($type->getMaxHP());
				$entityManager->persist($enemyShip);
				$cpt++;
			}
		}
		$entityManager->persist($sector);
	}
	
	static function buildFinalSector($player)
	{
		global $entityManager;
		$i18n = new I18n();
		$i18n->autoSetLang();
		$sector = new Sector($player,null,null,true);
		$lvl = $player->getEnemyColonyLevel();
		$min = FINAL_MIN_ENEMY_SHIPS * (1 + $lvl * FINAL_SECTOR_RATIO_MIN_MAX);
		$max = FINAL_MAX_ENEMY_SHIPS * (1 + $lvl * FINAL_SECTOR_RATIO_MIN_MAX);
		Builder::populateSector($sector,rand($min,$max));
		// we add at least one final boss in each sector for each level
		$bossType = $entityManager->getRepository('EnnemyShipType')->find(BOSS_ENEMY_SHIP_TYPE_ID);
		for ($i=0;$i <= $lvl; $i++)
		{
			$ennemyShip = new EnnemyShip($sector,$bossType,'Capital');
			$ennemyShip->setHP($bossType->getMaxHP());
			$entityManager->persist($ennemyShip);
		}
		$entityManager->persist($sector);
	}

	static function buildAllCharacters($fleet)
	{
		global $entityManager;
		for ($i = 1; $i <= NB_CHARACTERS; $i++)
		{
			$character = Builder::buildRandomCharacter($fleet);
			$entityManager->persist($character);
		}
	}

	static function restart($player)
	{
		global $entityManager;
		$player->setObjectiveType(0);
		$fleet = $player->getFleet();
		Builder::reinitFleetStocks($fleet);
		$fleet->setAdmiralShip(null);
		$fleet->setChief(null);
		
		$characters = $fleet->getCharacters();
		foreach($characters as $character)
		{
			$entityManager->remove($character);
		}
		
		$ships = $fleet->getShips();
		foreach ($ships as $ship)
		{
			$entityManager->remove($ship);
		}
		Builder::populateFleet($fleet,true);
		$fleet->setJumpStatus(JUMP_STATUS_IDLE);
		$fleet->setPoliticalSystem(null);
		$fleet->globalProtection();
		$sector = $player->getSector();

		Builder::freeSector($sector);

		$sector = Builder::buildSector($player,true);

		$entityManager->persist($sector);

		Builder::buildAllCharacters($fleet);

		$qb = $entityManager->createQueryBuilder();

		$qb->select('m')
		->from ('Message', 'm')
		->where ('m.recipient = :player and m.sender is null')
		->setParameter('player',$player);

		$query = $qb->getQuery();
		$notifications = $query->getResult();
		foreach ($notifications as $notification)
		{
			$entityManager->remove($notification);
		}

		$player->restart();
	}

	static function freeSector($sector)
	{
		global $entityManager;
		$ennemies = $sector->getEnnemies();
		$planet = $sector->getPlanet();
		$nexus = $sector->getNexus();
		if (isset($planet))
		{
			$newgarrison = array();
			foreach($ennemies as $ennemy)
			{
				$type = $ennemy->getType();
				$nb = $newgarrison[$type->getId()];
				if (empty($nb))
				{
					$nb = 0;
				}
				$newgarrison[$type->getId()] = $nb + 1;
			}
			$planet->setGarrison($newgarrison);
			$planet->unattack();
			if (!empty($newgarrison))
			{
				$planet->setStatus(1);
			}
			$planet->setSector(null);
		}
		if (isset($nexus))
		{
			$newgarrison = array();
			foreach($ennemies as $ennemy)
			{
				$type = $ennemy->getType();
				$nb = $newgarrison[$type->getId()];
				if (empty($nb))
				{
					$nb = 0;
				}
				$newgarrison[$type->getId()] = $nb + 1;
			}
			$nexus->setGarrison($newgarrison);
			$nexus->unattack();
			if (!empty($newgarrison))
			{
				$nexus->setStatus(1);
			}
			$nexus->setSector(null);
		}
		foreach($ennemies as $ennemy)
		{
			$entityManager->remove($ennemy);
		}
		$entityManager->remove($sector);
		$entityManager->flush();
	}
	
	static function resetVictory($player)
	{
		global $entityManager;
		global $i18n;
		$player->resetVictory();
        $sector = $player->getSector();
        Builder::freeSector($sector);
        $sector = Builder::buildSector($player);
        $entityManager->persist($sector);
        Tools::setFlashMsg($i18n->getText('msg.fleet.continue'));
	}
	
	static function jump($player,$withennemies=true,$planet=null,$nexus=null,$finalsector=false)
	{
		global $entityManager;
		global $i18n;
		$fleet = $player->getFleet();
		$jumpstatus = $fleet->getJumpStatus();
		if ($finalsector)
		{
			$jumpstatus = JUMP_STATUS_SAFE;
		}
		$admiral = $fleet->getAdmiralShip();
		$jumped = false;
		if (!is_null($admiral) && $admiral->getStaff() == 0 && !$finalsector)
		{
			Tools::setFlashMsg($i18n->getText('msg.admiral.ship.unable.to.jump'));
		}
		else
		{
			$nbshiplost = 0;
			$ships = $fleet->getShips();
			$ids = array();
			foreach ($ships as $ship)
			{
				if (is_null($admiral) || $fleet->getAdmiralShip()->getId() != $ship->getId())
				{
					array_push($ids,$ship->getId());
				}
			}

			if ($jumpstatus == MIN_JUMP_STATUS_FOR_JUMP)
			{
				$nbshiplost = round(count($ids) * 0.75,0);
			}
			elseif ($jumpstatus == JUMP_STATUS_SAVE_50_PERCENT)
			{
				$nbshiplost = round(count($ids) * 0.5,0);
			}
			elseif ($jumpstatus == JUMP_STATUS_SAVE_75_PERCENT)
			{
				$nbshiplost = round(count($ids) * 0.25,0);
			}
			elseif ($jumpstatus >= JUMP_STATUS_SAFE)
			{
				$nbshiplost = 0;
			}
			else
			{
				throw new Exception("Not allowed to jump");
			}
			
			if (!$finalsector)
			{

				$fleet->decreaseFuel(count($ships));
				$fleet->decreaseMoral($nbshiplost);

				Tools::setFlashMsg($i18n->getText('msg.jump.complete'));
				
				if ($nbshiplost > 0)
				{
					Tools::setFlashMsg($i18n->getText('msg.jump.lost.ships'));
					$lostships = array();
					for ($i=0;$i<$nbshiplost;$i++)
					{
						$lostShip = $fleet->getShip($ids[rand(0,count($ids)-1)]);
						array_push($lostships,array('id'=>$lostShip->getId(),'name'=>$lostShip->getName(),'type'=>$lostShip->getType()->getName()));
						$fleet->removeShip($lostShip);
						Helper::destroyShip($lostShip);
						$entityManager->remove($lostShip);
						$ids = array();
						foreach ($ships as $ship)
						{
							array_push($ids,$ship->getId());
						}
					}
					foreach ($lostships as $lostship)
					{
						Tools::setFlashMsg($i18n->getText('msg.jump.lost.ship',array($lostship['name'],$i18n->getText($lostship['type']))));
					}
				}
				else
				{
					Tools::setFlashMsg($i18n->getText('msg.jump.no.lost.ships'));
				}

				// we check if the ships can jump or not
				$ships = $fleet->getShips();
				foreach ($ships as $ship)
				{
					if ($ship->getStaff() == 0 || !$ship->getFTLDrive())
					{
						Tools::setFlashMsg($i18n->getText('msg.ship.unable.to.jump',array($ship->getName(),$i18n->getText($ship->getType()->getName()))));
						Helper::destroyShip($ship);
						$fleet->removeShip($ship);
						$entityManager->remove($ship);
					}
				}
			}

			$fleet->setJumpStatus(JUMP_STATUS_IDLE);
			$sector=$player->getSector();
			Builder::freeSector($sector);
			if (!is_null($planet))
			{
				Builder::buildPlanetSector($player,$planet);
			}
			elseif (!is_null($nexus))
			{
				Builder::buildNexusSector($player,$nexus);
			}
			elseif ($finalsector)
			{
				Builder::buildFinalSector($player);
			}
			else
			{
				$sector = Builder::buildSector($player,false,$withennemies);
				$entityManager->persist($sector);
			}
			$jumped = true;
		}
		return $jumped;
	}
}
