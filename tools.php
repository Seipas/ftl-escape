<?php

require_once(__DIR__."/bootstrap.php");
require_once __DIR__."/const.php";

class Tools {

    static function system_hack($info=array())
    {
?>
        <html>
            <head>
                <title>Hack detected</title>
            </head>
            <body>
                It appears you tried to hack the system. The incident has been reported.<br />
                <strong>Your account is now desactivated for security reasons.</strong><br />
                If you didn't attempt to corrupt the system, feel free to contact us.
            </body>
        </html>
<?php
        exit(42);
    }

    static function login_exists($login)
	{
		global $entityManager;
		$test = $entityManager->getRepository('Player')->findOneByLogin($login);
		return !is_null($test);
	}

	static function gameOver($player)
	{
		global $entityManager;
		$player->gameOver();
		$entityManager->flush();
	}

	static function getFreeRandomCoordinates($sourcex=0,$sourcey=0,$sourcez=0,$size=MAP_SIZE)
	{
		global $entityManager;
		$x=rand($sourcex-$size,$sourcex+$size);
		$y=rand($sourcey-$size,$sourcey+$size);
		$z=0;
		$repository = $entityManager->getRepository('Planet');
		$planet = $repository->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
		$repositoryfleet = $entityManager->getRepository('Fleet');
		$fleet = $repositoryfleet->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
		while (isset($planet) || isset($fleet))
		{
			$x=rand($sourcex-$size,$sourcex+$size);
			$y=rand($sourcey-$size,$sourcey+$size);
			$z=0;
			$planet = $repository->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
			$fleet = $repositoryfleet->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
			echo "Testing $x/$y/$z";
		}
		return array('x'=>$x,'y'=>$y,'z'=>$z);
	}

    static function randomString($car)
    {
        $string = "";
        $chaine = 'abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    static function remove_accents($str, $charset='UTF-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset, false);
        return $str;
    }

    static function enum($array, $asBitwise = FALSE) {

            if($array === null)             return FALSE;
            if(!is_array($array))           return FALSE;

            $count = 0;

            foreach($array as $i):
                if($i === null):
                    if($count == 0)      define($i, 0);
                    else                define($i, ($asBitwise === true) ? 1 << ($count - 1) : $count);
                endif;
                $count++;
            endforeach;

    }

    static function setFlashMsg($msg, $color="black")
    {
		$msg = '<span class="color-' . $color . '">' . $msg . '</span>';
        if (!isset($_SESSION['flash_message']))
            $_SESSION['flash_message'] = $msg;
        else
			$_SESSION['flash_message'] .= '<br>' . $msg;
    }

    static function getFlashMsg()
    {
		$msg='';
		if (isset($_SESSION['flash_message']))
		{
			$msg = $_SESSION['flash_message'];
			unset($_SESSION['flash_message']);
		}
        return $msg;
    }

    static function startsWith($haystack, $needle)
	{
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	static function endsWith($haystack, $needle)
	{
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}

	static function getFormatedNumber($number, $decPoint='.', $thousandsSep=' ')
	{
	       return number_format($number, 0, $decPoint, $thousandsSep);
	}

	// from php.net
	static function roman_numerals($input_arabic_numeral=0) {
		$arabic_numeral            = intval($input_arabic_numeral);
		$arabic_numeral_text    = "$arabic_numeral";
		$arabic_numeral_length    = strlen($arabic_numeral_text);

		if (!preg_match('/[0-9]/', $arabic_numeral_text)) {
			return false; }

		if ($arabic_numeral > 4999) {
			return false; }

		if ($arabic_numeral < 1) {
			return false; }

		if ($arabic_numeral_length > 4) {
			return false; }

		$roman_numeral_units    = $roman_numeral_tens        = $roman_numeral_hundreds        = $roman_numeral_thousands        = array();
		$roman_numeral_units[0]    = $roman_numeral_tens[0]    = $roman_numeral_hundreds[0]    = $roman_numeral_thousands[0]    = ''; // NO ZEROS IN ROMAN NUMERALS

		$roman_numeral_units[1]='I';
		$roman_numeral_units[2]='II';
		$roman_numeral_units[3]='III';
		$roman_numeral_units[4]='IV';
		$roman_numeral_units[5]='V';
		$roman_numeral_units[6]='VI';
		$roman_numeral_units[7]='VII';
		$roman_numeral_units[8]='VIII';
		$roman_numeral_units[9]='IX';

		$roman_numeral_tens[1]='X';
		$roman_numeral_tens[2]='XX';
		$roman_numeral_tens[3]='XXX';
		$roman_numeral_tens[4]='XL';
		$roman_numeral_tens[5]='L';
		$roman_numeral_tens[6]='LX';
		$roman_numeral_tens[7]='LXX';
		$roman_numeral_tens[8]='LXXX';
		$roman_numeral_tens[9]='XC';

		$roman_numeral_hundreds[1]='C';
		$roman_numeral_hundreds[2]='CC';
		$roman_numeral_hundreds[3]='CCC';
		$roman_numeral_hundreds[4]='CD';
		$roman_numeral_hundreds[5]='D';
		$roman_numeral_hundreds[6]='DC';
		$roman_numeral_hundreds[7]='DCC';
		$roman_numeral_hundreds[8]='DCCC';
		$roman_numeral_hundreds[9]='CM';

		$roman_numeral_thousands[1]='M';
		$roman_numeral_thousands[2]='MM';
		$roman_numeral_thousands[3]='MMM';
		$roman_numeral_thousands[4]='MMMM';

		if ($arabic_numeral_length == 3) { $arabic_numeral_text = "0" . $arabic_numeral_text; }
		if ($arabic_numeral_length == 2) { $arabic_numeral_text = "00" . $arabic_numeral_text; }
		if ($arabic_numeral_length == 1) { $arabic_numeral_text = "000" . $arabic_numeral_text; }

		$anu = substr($arabic_numeral_text, 3, 1);
		$anx = substr($arabic_numeral_text, 2, 1);
		$anc = substr($arabic_numeral_text, 1, 1);
		$anm = substr($arabic_numeral_text, 0, 1);

		$roman_numeral_text = $roman_numeral_thousands[$anm] . $roman_numeral_hundreds[$anc] . $roman_numeral_tens[$anx] . $roman_numeral_units[$anu];
		return ($roman_numeral_text);
	}
}
