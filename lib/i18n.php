<?php

require_once(__DIR__.'/../const.php');

class I18n {

	private $langs = array(
        'fr'=>true,
		'en'=>false,
	);

	private $i18n;
	private $lang;

	public function autoSetLang()
	{
		$lang = HTTP::negotiateLanguage($this->langs);
		if (empty($lang))
		{
			$lang = DEFAULT_LANGUAGE;
		}
		$this->setLang($lang);
	}

	public function setLang($lang)
	{
		if(is_file(__DIR__.'/../lang/'. $lang .'.ini')){
			$this->lang = $lang;
		}
		else{
			$this->lang = DEFAULT_LANGUAGE;
		}
		$this->i18n = parse_ini_file(__DIR__.'/../lang/'. $this->lang .'.ini');
	}

	public function getLang()
	{
		return $this->lang;
	}

	public function getText($key,$replacements=null)
	{
		$text = $this->i18n[$key];
		if (!is_null($replacements))
		{
			$indexes = array();
			foreach ($replacements as $key=>$value)
			{
				$val = '{'.$key.'}';
				array_push($indexes,$val);
				$replacements[$val]=$value;
			}
			$text = str_replace($indexes,$replacements,$text);
		}
		return $text != '' ? $text : $key;
	}
}
