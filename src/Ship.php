<?php
require_once __DIR__.'/../const.php';
// should extend ShipType
/**
 * @Entity @Table(name="ships")
 **/
class Ship
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @ManyToOne(targetEntity="ShipType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $hp;
    /** @ManyToOne(targetEntity="Fleet", inversedBy="ships")
     * @var Fleet
     */
    private $fleet;
    /** @Column(type="integer") **/
    protected $passengers;
    /** @Column(type="integer") **/
    protected $staff;
    /** @Column(type="boolean") **/
    protected $FTLdrive=true;
    /** @Column(type="boolean") **/
    protected $surviveSystems=true;
    /** @Column(type="integer") **/
    protected $oxygenLevel=100;
    /** @Column(type="integer") **/
    protected $level=0;
    /** @ManyToOne(targetEntity="Fleet", inversedBy="protectedShips")
     * @var Fleet
     **/
     private $protectedByFleet;
	 /**
     * @OneToMany(targetEntity="Character", mappedBy="ship", indexBy="id")
     * @var Character[]
     **/
    private $characters;
    
    public function __construct($fleet,$type)
    {
		$this->fleet=$fleet;
		$this->type=$type;
		$fleet->addShip($this);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getFleet()
	{
		return $this->fleet;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setHP($hp)
	{
		$this->hp=$hp;
	}
	
	public function getHP()
	{
		return $this->hp;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setPassengers($passengers)
	{
		if ($passengers > $this->type->getMaxPassengers())
		{
			throw new Exception("Too much passengers for this ship");
		}
		$this->passengers = $passengers;
	}
	
	public function getPassengers()
	{
		return $this->passengers;
	}
	
	public function setStaff($staff)
	{
		if ($staff > $this->type->getQualifiedStaff())
		{
			throw new Exception("Too much staff for this ship");
		}
		$this->staff = $staff;
	}
	
	public function getStaff()
	{
		return $this->staff;
	}
	
	public function takeDamage($damage)
	{
		$defense = $this->getDefense($this->level);
		if (!is_null($this->fleet->getAdmiralShip()) && $this->fleet->getAdmiralShip()->getId() == $this->getId() && count($this->fleet->getShips()) > 1)
		{
			$defense += ADMIRAL_SHIP_DEFENSE_BONUS;
		}
		$damage -= $defense;
		if ($damage > 0)
		{
			$this->hp -= $damage;
		}
		if ($this->hp <= 0)
		{
			$this->fleet->removeShip($this);
		}
	}
	
	public function getLevel()
	{
		return $this->level;
	}
	
	public function setLevel($level)
	{
		$this->level = $level;
	}
	
	public function getEfficiency()
	{
		if ($this->type->getQualifiedStaff() == 0)
		{
			return 1;
		}
		return round($this->staff / $this->type->getQualifiedStaff(),3);
	}
	
	public function getDefense($level=null)
	{
		if (is_null($level))
		{
			$level = $this->getLevel();
		}
		$bonus = 1;
		$objective = $this->getFleet()->getPlayer()->getObjectiveType();
		if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
		{
			$bonus = OBJECTIVE_SEARCH_HABITABLE_DEFENSE_BONUS;
		}
		elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
		{
			$bonus = OBJECTIVE_SEARCH_EARTH_DEFENSE_BONUS;
		}
		
		$defense = 0;
		$efficiency = $this->getEfficiency();
		
		if ($this->fleet->getSpecificProtection())
		{
			if (!is_null($this->fleet->getProtectedShips()[$this->getId()]))
			{
				$defense = $this->fleet->getSpecificDefenseValue();
				$efficiency = 1;
			}
			else
			{
				$defense = 0;
			}
		}
		else
		{
			$defense = $this->type->getDefense($level);
		}
		
		$fleetBonus=1;
		$politicalsystem = $this->fleet->getPoliticalSystem();
		if (!is_null($politicalsystem))
		{
			$fleetBonus = 1 + $politicalsystem->getDefenseBonus();
		}
		
		// the defense buff is only applicable to the ship, not for the fleet
		$charactersBonus=1;
		$characters = $this->characters;
		foreach ($characters as $character)
		{
			$charactersBonus += $character->getDefenseBonus();
		}
		return round($defense * $efficiency * $bonus * $fleetBonus * $charactersBonus);
	}
	
	public function canJump()
	{
		return $this->getStaff() != 0 && $this->getFTLDrive();
	}
	
	public function getFTLDrive()
	{
		return $this->FTLdrive;
	}
	
	public function getSurviveSystems()
	{
		return $this->surviveSystems;
	}
	
	public function breakFTL()
	{
		$this->FTLdrive=false;
	}
	
	public function breakSurviveSystems()
	{
		$this->surviveSystems=false;
	}
	
	public function repairFTL()
	{
		$this->FTLdrive=true;
	}
	
	public function repairSurviveSystems()
	{
		$this->surviveSystems=true;
	}
	
	public function oxygen()
	{
		if (!$this->surviveSystems)
		{
			$capacity = $this->getType()->getMaxPassengers() + $this->getType()->getQualifiedStaff();
			$population = $this->getStaff() + $this->getPassengers();
			$level = round($population / $capacity,2);
			$consumption = round(OXYGEN_CONSUMPTION * $level);
			if ($consumption == 0)
			{
				$consumption = 1;
			}
			$this->oxygenLevel -= $consumption;
			if ($this->oxygenLevel < 0)
			{
				$this->oxygenLevel = 0;
			}
			if ($this->oxygenLevel == 0)
			{
				$this->staff = 0;
				$this->passengers = 0;
			}
			if ($this->oxygenLevel <= DEADLY_OXYGEN_LEVEL)
			{
				$percentDeath = round((DEADLY_OXYGEN_LEVEL - $this->oxygenLevel) / DEADLY_OXYGEN_LEVEL,2);
				if ($percentDeath == 0)
				{
					$percentDeath = 0.01;
				}
				$this->staff = round($this->staff * (1 - $percentDeath));
				$this->passengers = round($this->passengers * (1 - $percentDeath));
			}
		}
		else
		{
			$this->oxygenLevel = 100;
		}
	}
	
	public function getOxygenLevel()
	{
		return $this->oxygenLevel;
	}
	
	public function getAttack($level)
	{
		$bonus = 1;
		$objective = $this->getFleet()->getPlayer()->getObjectiveType();
		if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
		{
			$bonus = OBJECTIVE_SEARCH_HABITABLE_ATTACK_BONUS;
		}
		elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
		{
			$bonus = OBJECTIVE_SEARCH_EARTH_ATTACK_BONUS;
		}
		$fleetBonus=1;
		$politicalsystem = $this->fleet->getPoliticalSystem();
		if (!is_null($politicalsystem))
		{
			$fleetBonus = 1 + $politicalsystem->getAttackBonus();
		}
		$charactersBonus = 1;
		$characters = $this->characters;
		foreach ($characters as $character)
		{
			$charactersBonus += $character->getAttackBonus();
		}
		return round($this->type->getAttack($level) * $this->getEfficiency() * $bonus * $fleetBonus * $charactersBonus);
	}
	
	public function setProtection($fleet)
	{
		$this->protectedByFleet = $fleet;
	}
	
	public function getCharacters()
	{
		return $this->characters->toArray();
	}
	
	public function getCharacter($id)
	{
		if (!isset($this->characters[$id]))
		{
			return false;
		}
		else
		{
			return $this->characters[$id];
		}
	}
	
	public function addCharacter($character)
	{
		$this->characters[$character->getId()] = $character;
	}
	
	public function getMaxHP()
	{
		return $this->getType()->getMaxHP($this->getLevel());
	}
}
