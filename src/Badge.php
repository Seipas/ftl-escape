<?php
require_once __DIR__.'/../const.php';
/**
 * @Entity @Table(name="badges")
 **/
class Badge
{
    /** @Id @Column(type="integer") **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="text", nullable=true) **/
    protected $icon;
    /** @Column(type="text", nullable=true) **/
    protected $badge_type; // 'player' or 'fleet', 'fleet' not implemented
    /** @Column(type="array", nullable=true) **/
    protected $conditions;
    
    /* conditions format :
      * array (
      * 		"moral"=>INTEGER,
      * 		"fuel"=>INTEGER,
      * 		"food"=>INTEGER,
      * 		"medicine"=>INTEGER,
      * 		"material"=>INTEGER,
      * 		"nbships"=>INTEGER,
      * 		"survivors"=>INTEGER, // not implemented
      * 		"globalattack"=>INTEGER, // not implemented
      * 		"globaldefense"=>INTEGER, // not implemented
      * 		"killed"=>INTEGER,
      * 		"victoriousnewplanet"=>BOOLEAN,
      * 		"victoriousearth"=>BOOLEAN,
      * 		"freedcolony"=>BOOLEAN,
      * 		"destroyednexus"=>BOOLEAN,
      * 		)
      */
    
    public function __construct($id,$name,$icon,$type,$conditions)
    {
        $this->id = $id;
        $this->name = $name;
        $this->icon = $icon;
        $this->badge_type=$type;
        $this->conditions=$conditions;
    }
    
    public function getId()
    {
		return $this->id;
	}
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getIcon()
    {
        return $this->icon;
    }
    
    public function getDescription()
    {
        return $this->name.'.description';
    }
    
    public function matchConditions($player,$em)
	{
		if (is_null($em))
		{
			throw new Exception('EntityManager is null');
		}
		if (is_null($this->conditions))
		{
			return true;
		}
		$conditions = $this->conditions;
		$fleet = $player->getFleet();
		$sector = $player->getSector();
		$colonies = $em->getRepository('Planet')->findBy(array('freedBy'=>$player));
		$nexii = $em->getRepository('Nexus')->findBy(array('destroyedBy'=>$player));
		$matchConditions = true;
		foreach ($conditions as $key=>$value)
		{
			switch ($key)
			{
				case 'moral': $matchConditions = $matchConditions && $fleet->getMoral() >= $value; break;
				case 'fuel': $matchConditions = $matchConditions && $fleet->getFuel() >= $value; break;
				case 'food': $matchConditions = $matchConditions && $fleet->getFood() >= $value; break;
				case 'medicine': $matchConditions = $matchConditions && $fleet->getMedicine() >= $value; break;
				case 'material': $matchConditions = $matchConditions && $fleet->getMaterial() >= $value; break;
				case 'nbships': $matchConditions = $matchConditions && count($fleet->getShips()) >= $value; break;
                case 'killed': $matchConditions = $matchConditions && $fleet->getNbEnemiesKilled() >= $value; break;
				case 'victoriousnewplanet': $matchConditions = $matchConditions && $player->isVictorious() && $player->getObjectiveType() == OBJECTIVE_SEARCH_HABITABLE_ID;break;
				case 'victoriousearth': $matchConditions = $matchConditions && $player->isVictorious() && $player->getObjectiveType() == OBJECTIVE_SEARCH_EARTH_ID && $player->getEarthClues() == NB_CLUES_TO_EARTH;break;
				case 'freedcolony': $matchConditions = $matchConditions && count($colonies) > 0;break;
				case 'destroyednexus': $matchConditions = $matchConditions && count($nexii) > 0; break;
			}
		}
		return $matchConditions;
	}
    
    public function getType()
    {
        return $this->badge_type;
    }
}
