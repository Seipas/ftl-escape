<?php
require_once __DIR__.'/../const.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="characters")
 **/
class Character
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $name;
    /** @ManyToOne(targetEntity="PoliticalSystem") **/
    private $politicalsystem;
    /** @ManyToOne(targetEntity="Fleet", inversedBy="characters")
     * @var Fleet
     */
    private $fleet;
    /** @Column(type="integer",options={"default"=50}) **/
    protected $popularity=50;
    /** @Column(type="string", nullable=true) **/
    protected $avatar;
    /** @Column(type="string")**/
    protected $type;
    /** @Column(type="float", options={"default"=0}) **/
    protected $bonusattack=0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $bonusdefense=0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $bonusmoral=0;
    /** @Column(type="integer") **/
    protected $gender=50;
    /** @ManyToOne(targetEntity="Ship", inversedBy="characters")
     * @var Ship
     */
    private $ship;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $xp=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $lvl=0;
    /** @Column(type="integer",options={"default"=100}) **/
    protected $hp=100;
    
    public function __construct($fleet,$ship,$name,$type)
    {
        $this->fleet = $fleet;
        $this->name = $name;
        $this->type = $type;
        $this->ship = $ship;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setShip($ship)
    {
        $this->ship = $ship;
    }
    
    public function getShip()
    {
        return $this->ship;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setPoliticalSystem($ps)
    {
        $this->politicalsystem = $ps;
    }
    
    public function getPoliticalSystem()
    {
        return $this->politicalsystem;
    }
    
    public function getFleet()
    {
        return $this->fleet;
    }
    
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }
    
    public function getPopularity()
    {
        return $this->popularity;
    }
    
    public function modifyPopularity($value)
    {
		$this->popularity+=$value;
		if ($this->popularity > 100)
		{
			$this->popularity=100;
		}
		if ($this->popularity < 0)
		{
			$this->popularity=0;
		}
	}
    
    public function setType($type)
    {
        $this->type = $type;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    public function setAttackBonus($bonus)
    {
        $this->bonusattack = $bonus;
    }
    
    public function setDefenseBonus($bonus)
    {
        $this->bonusdefense = $bonus;
    }
    
    public function setMoralBonus($bonus)
    {
        $this->bonusmoral = $bonus;
    }
    
    public function getAttackBonus()
    {
        return $this->bonusattack;
    }
    
    public function getDefenseBonus()
    {
        return $this->bonusdefense;
    }
    
    public function getMoralBonus()
    {
        return $this->bonusmoral;
    }
    
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
    
    public function getGender()
    {
        return $this->gender;
    }
    
}
