<?php
require_once __DIR__.'/../const.php';
if(! function_exists('password_hash')) {
	function password_hash($password,$PASSWORD_DEFAULT=null){
		$salt ='$2a$' . str_pad(8, 2, '0', STR_PAD_LEFT) . '$' .substr(strtr(base64_encode(openssl_random_pseudo_bytes(16)), '+', '.'),0, 22);
		return crypt($password, $salt);
	}
}
if(! function_exists('password_verify')) {
	function password_verify($password,$hash){
		return crypt($password, $hash) == $hash;
	}
}

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="players")
 **/
class Player
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string",unique=true) **/
    protected $login;
    /** @Column(type="string") **/
    protected $password;
    /** @Column(type="boolean") **/
    protected $npc = false;
    /** @OneToOne(targetEntity="Fleet", mappedBy="player") **/
    private $fleet;
    /** @OneToOne(targetEntity="Sector", mappedBy="player") **/
    private $sector;
    /** @Column(type="boolean") **/
    protected $gameover = false;
    /** @Column(type="integer") **/
    protected $objectivetype = 0;
    /** @Column(type="string",nullable=true) **/
    protected $email;
    /** @Column(type="integer", nullable=true) **/
    protected $deletiondate;
    /** @Column(type="integer") **/
    protected $earth_clues=0;
    /** @Column(type="boolean") **/
    protected $victory=false;
    /** @ManyToOne(targetEntity="EventType") **/
    private $event;
	/** @Column(type="integer", nullable=true) **/
	protected $difficulty;
	/** @ManyToMany(targetEntity="Badge",indexBy="id")
	 * @var Badge[]
	 */
	private $badges;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $lastattack=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $lastlogin=0;
	/** @Column(type="integer", options={"default"=0}) **/
	protected $assassinationcount=0;
	/** @Column(type="array",nullable=true) **/
	protected $globalannouncements=array();
	/** @Column(type="integer", options={"default"=0}) **/
	protected $enemycolonylevel=0;
	/** @Column(type="boolean", options={"default"=0}) **/
    protected $showAsTable=false;
    /** @Column(type="boolean", options={"default"=0}) **/
    protected $deleted = false;
	
	public function __construct()
	{
		$this->badges = new ArrayCollection();
	}
	
    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }
	
	public function getName()
	{
		return $this->getLogin();
	}

    public function setLogin($login)
    {
        $this->login = $login;
    }
    
    public function getPassword()
    {
		return $this->password;
	}
	
	public function setPassword($password)
	{
		$this->password = password_hash($password,PASSWORD_DEFAULT);
	}
	
	public function checkPassword($password)
	{
		if ($this->deleted)
		{
			return false;
		}
		else
		{
			return password_verify($password,$this->password) == $this->password;
		}
	}
	
	public function getFleet()
	{
		return $this->fleet;
	}
	
	public function setFleet($fleet)
	{
		$this->fleet=$fleet;
	}
	
	public function setSector($sector)
	{
		$this->sector=$sector;
	}
	
	public function getSector()
	{
		return $this->sector;
	}
	
	public function gameOver()
	{
		$this->gameover = true;
	}
	
	public function isGameOver()
	{
		return $this->gameover;
	}
	
	public function restart()
	{
		$this->gameover = false;
		$this->victory = false;
		$this->earth_clues = 0;
	}
	
	public function getObjectiveType()
	{
		return $this->objectivetype;
	}
	
	public function setObjectiveType($type)
	{
		$this->objectivetype=$type;
	}
	
	public function setEmail($email)
	{
		if (filter_var($email,FILTER_VALIDATE_EMAIL))
		{
			$this->email = $email;
		}
	}
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function deleteAccount()
	{
		$this->deletiondate=time();
	}
	
	public function getDeletionDate()
	{
		return $this->deletiondate;
	}
	
	public function abortDeletion()
	{
		$this->deletiondate=null;
	}
	
	public function getEarthClues()
	{
		return $this->earth_clues;
	}
	
	public function findEarthClue()
	{
		$this->earth_clues += 1;
		if ($this->earth_clues > NB_CLUES_TO_EARTH)
		{
			$this->earth_clues = NB_CLUES_TO_EARTH;
		}
	}
	
	public function isVictorious()
	{
		return $this->victory;
	}
	
	public function setAsVictorious()
	{
		$this->victory = true;
	}
	
	public function resetVictory()
	{
		$this->victory = false;
		$this->earth_clues = 0;
		$this->objectivetype = 0;
	}
	
	public function getEvent()
	{
		return $this->event;
	}
	
	public function setEvent($eventType)
	{
		$this->event = $eventType;
	}
	
	public function getDifficulty()
	{
		return $this->difficulty;
	}
	
	public function setDifficulty($difficulty)
	{
		if (!is_null($difficulty) && $difficulty > MAX_DIFFICULTY)
		{
			$this->difficulty = MAX_DIFFICULTY;
			return;
		}
		$this->difficulty = $difficulty;		
	}
	
	public function getBadges()
	{
		return $this->badges->toArray();
	}
	
	public function getBadge($id)
	{
		if (!isset($this->badges[$id]))
		{
			echo "not in badges";
		}
		else
		{
			return $this->badges[$id];
		}
	}
	
	public function addBadge(Badge $badge)
	{
		// we could use "matchConditions()" here, but this would prevent manual badges attribution
		$this->badges[$badge->getId()] = $badge;
	}
	
	public function removeBadge($badge)
	{
		
	}
	
	public function setLastAttack($time)
	{
		$this->lastattack = $time;
	}
	
	public function setLastLogin($time)
	{
		$this->lastlogin = $time;
	}
	
	public function getLastAttack()
	{
		return $this->lastattack;
	}
	
	public function getLastLogin()
	{
		return $this->lastlogin;
	}
	
	public function setAssassinationCount($value)
	{
		$this->assassinationcount = $value;
	}
	
	public function getAssassinationCount()
	{
		return $this->assassinationcount;
	}
	
	public function addGlobalAnnouncement($value)
	{
		if (empty($this->globalannouncements))
		{
			$this->globalannouncements = array();
		}
		array_push($this->globalannouncements,$value);
	}
	
	public function getGlobalAnnouncements()
	{
		return $this->globalannouncements;
	}
	
	public function resetGlobalAnnouncements()
	{
		$this->globalannouncements = array();
	}
	
	public function setGlobalAnnouncements($value)
	{
		$this->globalannouncements = $value;
	}
	
	public function setEnemyColonyLevel($value)
	{
		$this->enemycolonylevel=$value;
	}
	
	public function getEnemyColonyLevel()
	{
		return $this->enemycolonylevel;
	}
	
	public function setShowAsTable($value)
	{
		$this->showAsTable=$value;
	}
	
	public function getShowAsTable()
	{
		return $this->showAsTable;
	}
	
	public function trueDeleteAccount()
	{
		$this->deleted = true;
	}
	
	public function isDeleted()
	{
		return $this->deleted;
	}
}
