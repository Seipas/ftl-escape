<?php
/**
 * @Entity @Table(name="ennemyshiptypes")
 **/
class EnnemyShipType
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="integer") **/
    protected $maxHp=1;
    /** @Column(type="integer") **/
    protected $defense=0;
    /** @Column(type="integer") **/
    protected $attack=0;
    /** @Column(type="integer") **/
    protected $difficulty;
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getDescription()
	{
		return $this->name.'.description';
	}
	
	public function setAttack($damage)
	{
		$this->attack = $damage;
	}
	
	public function getAttack()
	{
		return $this->attack;
	}
	
	public function setMaxHP($hp)
	{
		$this->maxHp=$hp;
	}
	
	public function getMaxHP()
	{
		return $this->maxHp;
	}
	
	public function setDefense($defense)
	{
		$this->defense=$defense;
	}
	
	public function getDefense()
	{
		return $this->defense;
	}
	
	public function setDifficulty($diff)
	{
		$this->difficulty=$diff;
	}
	
	public function getDifficulty()
	{
		return $this->difficulty;
	}
}
