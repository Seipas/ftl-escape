<?php

require_once(__DIR__.'/const.php');
require_once(__DIR__.'/tools.php');

class Helper
{
	static function canScout($fleet)
	{
		$ships = $fleet->getShips();
		$canScout = false;
		foreach($ships as $ship)
		{
			$canScout = $canScout || $ship->getType()->canScout();
		}
		return $canScout;
	}

	static function calculateDifficulty($player)
	{
		$fleet = $player->getFleet();
		$attack = $fleet->getCombinedAttack();
		$ships = $fleet->getShips();
		$defense = 0;
		foreach($ships as $ship)
		{
			$defense += $ship->getDefense();
		}
		$difficulty = round(($attack * DIFFICULTY_CALC_MULTIPLIER_ATTACK + $defense * DIFFICULTY_CALC_MULTIPLIER_DEFENSE) / DIFFICULTY_CALC_DIVIDER,0);
		if ($difficulty <= 0)
		{
			$difficulty = 1;
		}
		return ($difficulty > MAX_DIFFICULTY) ? MAX_DIFFICULTY : $difficulty;
	}

	static function getDifficulty(Player $player)
	{
		$difficulty = $player->getDifficulty();
		return (is_null($difficulty)) ? Helper::calculateDifficulty($player) : $difficulty;
	}

	static function canProduce($fleet)
	{
		$ships = $fleet->getShips();
		$canProduce = false;
		while (!$canProduce && list(,$ship) = each($ships))
		{
			$canProduce = $canProduce || $ship->getType()->canProduceShips();
		}
		return $canProduce;
	}

	static function canRepair($fleet)
	{
		$ships = $fleet->getShips();
		$canRepair = false;
		while (!$canRepair && list(,$ship) = each($ships))
		{
			$canRepair = $canRepair || $ship->getType()->canRepair();
		}
		return $canRepair;
	}

	static function hasEvent($player)
	{
		return !is_null($player->getEvent());
	}

	static function canAct($player)
	{
		return !Helper::underAttack($player) && !Helper::hasEvent($player);
	}

	static function underAttack($player)
	{
		$sector = $player->getSector();
		return count($sector->getEnnemies()) > 0;
	}

	static function calculateSurvivors($player)
	{
		$survivors = 0;
		$ships = $player->getFleet()->getShips();
		foreach ($ships as $ship)
		{
			$survivors += $ship->getPassengers() + $ship->getStaff();
		}
		return $survivors;
	}

	static function generateCSRFToken()
	{
		global $entityManager;
		$token = new CSRF();
		$entityManager->persist($token);
		$entityManager->flush();
		return $token->getToken();
	}
	
	static function getShipTypeFromId($typeId)
	{
		global $entityManager;
		$type = $entityManager->find('ShipType',$typeId);
		return $type;
	}
	
	static function fleetHasShipType($fleet,$type)
	{
		global $entityManager;
		$qb = $entityManager->createQueryBuilder();
		$qb->select('s')
			->from('Ship','s')
			->where('s.type = :type')
			->andWhere('s.fleet = :fleet')
			->setParameter('type',$type)
			->setParameter('fleet',$fleet);
		return count($qb->getQuery()->getResult()) >= 1;
	}

	static function checkCSRF($token)
    {
		if(isset($token))
		{
			global $entityManager;
			$csrf = $entityManager->getRepository('CSRF')->find($token);
			return !is_null($csrf) && $csrf->isActive();
		}
		else
		{
			return false;
		}
    }

	static function checkBadges($player)
	{
		global $entityManager;
		$badges = $entityManager->getRepository('Badge')->findAll();
		foreach ($badges as $badge)
		{
			if ($badge->matchConditions($player,$entityManager))
			{
				if ($badge->getType() == 'player')
				{
					$player->addBadge($badge);
				}
				elseif ($badge->getType() == 'fleet')
				{
					// not implemented
				}
			}
		}
	}

	static function destroyShip($ship,$withMalus=true)
	{
		global $entityManager;
		global $i18n;
		$characters = $ship->getCharacters();
		$fleet = $ship->getFleet();
		foreach ($characters as $character)
		{
			if (!is_null($fleet->getChief()) && $fleet->getChief()->getId() == $character->getId())
			{
				$fleet->setChief(null);
			}
			if ($withMalus)
			{
				$fleet->decreaseMoral($character->getPopularity()*CHARACTER_POPULARITY_MORAL_MULTIPLIER);
				$txt='msg.character.dead.f';
				if ($character->getGender() < 50)
				{
					$txt = 'msg.character.dead.h';
				}
				Tools::setFlashMsg($i18n->getText($txt,array($character->getName())));
			}
			$entityManager->remove($character);
		}
		$fleet = $ship->getFleet();
		if ($withMalus)
		{
			$fleet->decreaseMoral(1);
		}
	}
	
	static function killRandomPopulation($fleet,$nb)
	{
		global $entityManager;
		$ships = $fleet->getShips();
		$max = $nb;
		$keys = array_rand($ships,count($ships));
		foreach ($keys as $key)
		{
			if ($max > 0)
			{
				$ship = $ships[$key];
				$kills = rand(0,$max);
				$passengers = $ship->getPassengers();
				if ($kills <= $passengers)
				{
					$ship->setPassengers($passengers - $kills);
				}
				else
				{
					$max += $kills - $passengers;
					$ship->setPassengers(0);
				}
				$max -= $kills;
			}
		}
	}
	
	static function goodAction($player)
	{
		$opinionbonus = rand(MIN_OPINION_BONUS_MALUS,MAX_OPINION_BONUS_MALUS);
		return Helper::opinionAction($player,$opinionbonus);
	}
	
	static function badAction($player)
	{
		$opinionmalus = 0-rand(MIN_OPINION_BONUS_MALUS,MAX_OPINION_BONUS_MALUS);
		return Helper::opinionAction($player,$opinionmalus);
	}
	
	static function opinionAction($player,$value)
	{
		$fleet = $player->getFleet();
		$chief = $fleet->getChief();
		if (!is_null($chief))
		{
			$system = $chief->getPoliticalSystem();
			$characters = $fleet->getCharacters();
			foreach ($characters as $character)
			{
				$ps = $character->getPoliticalSystem();
				if ($ps->getId() == $system->getId())
				{
					$character->modifyPopularity($value);
				}
				else
				{
					$ammount = round((0-$value) * OPPOSITION_MORAL_RATIO);
					$character->modifyPopularity($ammount);
				}
			}
		}
	}
}
