<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
	$fleet = $player->getFleet();
	$ship = $fleet->getShip($_POST['shipid']);

	function sanitize_ship_name($name)
	{
		return $name;
	}

	$newname = sanitize_ship_name($_POST['new_name']);
	$ship->setName($newname);
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
