<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$shipid = $_GET['id'];
	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}
	$fleet = $player->getFleet();

	$ship = $fleet->getShip($shipid);

	if (is_null($ship))
	{
		echo "This ship does not seems to be in this fleet. Please go back.";
		exit;
	}

	$canRepair = false;
	$ships = $fleet->getShips();
	foreach ($ships as $s)
	{
		$canRepair = $canRepair || $s->getType()->canRepair();
	}

	if ($canRepair)
	{
		$sector = $player->getSector();
		$ennemies = $sector->getEnnemies();

		if (count($ennemies) == 0 && $fleet->getMaterial() > 0)
		{
			$maxhp = $ship->getType()->getMaxHP($ship->getLevel());
			$repairAmmount = $maxhp - $ship->getHP();
			if ($repairAmmount > $fleet->getMaterial())
			{
				$repairAmmount = $fleet->getMaterial();
			}
			$ship->setHP($ship->getHP() + $repairAmmount);
			$ship->repairFTL();
			$ship->repairSurviveSystems();
			$fleet->decreaseMaterial($repairAmmount);
			Tools::setFlashMsg($i18n->getText('msg.ship.repaired',array($ship->getName())));
		}
		elseif ($fleet->getMaterial() == 0)
		{
			Tools::setFlashMsg($i18n->getText('msg.not.enough.material'));
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.not.in.safe.space'));
		}
	}
	else
	{
		echo "This fleet has no repair ability.";
		exit;
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
