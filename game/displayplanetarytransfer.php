<?php

$fleet = $player->getFleet();
$sector = $player->getSector();
$canAct = count($sector->getEnnemies()) == 0;
$smarty->assign('ships',$fleet->getShips());
$qb = $entityManager->createQueryBuilder();
$qb->select('p')
	->from('Planet','p')
	->where('p.freedBy = :player')
	->setParameter('player',$player);
$query = $qb->getQuery();
$planets = $query->getResult();
$smarty->assign('planets',$planets);


// i18n
$smarty->assign('lbl_transfer_title',$i18n->getText('lbl.transfer.title'));
$smarty->assign('lbl_transfer',$i18n->getText('lbl.transfer'));
$smarty->assign('lbl_ship_name',$i18n->getText('lbl.ship.name'));
$smarty->assign('lbl_ship_type_name',$i18n->getText('lbl.ship.type.name'));
$smarty->assign('lbl_ship_passengers',$i18n->getText('lbl.ship.passengers'));
$smarty->assign('lbl_ship_staff',$i18n->getText('lbl.ship.staff'));
$smarty->assign('lbl_max_passengers',$i18n->getText('lbl.ship.max.passengers'));
$smarty->assign('lbl_max_staff',$i18n->getText('lbl.ship.max.staff'));
$smarty->assign('lbl_nb_to_dispatch',$i18n->getText('lbl.nb.to.dispatch'));
$smarty->assign('lbl_operation',$i18n->getText('lbl.operation'));
$smarty->assign('msg_impossible',$i18n->getText('msg.impossible.action'));
$smarty->assign('can_act',$canAct);
$smarty->assign('i18n',$i18n);

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
