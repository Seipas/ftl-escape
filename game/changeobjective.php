<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
	$game = $entityManager->getRepository('Game')->find(1);

	$objective = $player->getObjectiveType();

	if ($game->getChooseObjective() && $objective == 0)
	{
		if (is_numeric($_POST['objective']) && $_POST['objective'] <= MAX_OBJECTIVE_ID && $_POST['objective'] > 0)
		{
			$objective = $_POST['objective'];
			$player->setObjectiveType($objective);
			if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
			{
				$player->getFleet()->increaseMoral(OBJECTIVE_SEARCH_HABITABLE_MORAL_BONUS);
			}
			elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
			{
				$player->getFleet()->increaseMoral(OBJECTIVE_SEARCH_EARTH_MORAL_BONUS);
			}
			elseif ($objective == OBJECTIVE_RETAKE_COLONIES_ID)
			{
				$player->getFleet()->increaseMoral(OBJECTIVE_RETAKE_COLONIES_MORAL_BONUS);
			}
		}
		else
		{
			echo "Nope.";
			exit;
		}
	}
	else
	{
		Tools::setFlashMsg($i18n->getText('msg.not.allowed'));
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header ('Location: index.php?page=politics');
