<?php
include __DIR__.'/../lib/session.inc.php';
check_login();
date_default_timezone_set('UTC');
require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');
require_once(__DIR__.'/../helper.php');

$smarty = new Smarty();

$smarty->setTemplateDir('tpl');
$smarty->setCompileDir('tmp');
$smarty->setConfigDir('conf');
$smarty->setCacheDir('cache');

$smarty->assign('username',$_SESSION['username']);
$smarty->setCaching(Smarty::CACHING_OFF);
$smarty->debugging = false;
//$smarty->setCompileCheck(false);

$i18n = new I18n();
$i18n->autoSetLang();

$game = $entityManager->getRepository('Game')->find(1);

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);
if (is_null($player))
{
	echo 'Account does not exist';
	exit;
}

if ($player->getFleet()->isEmpty() && !$player->isGameOver())
{
	Tools::gameOver($player);
}

if ($game->getStage() != STAGE_INITIAL && $player->getObjectiveType() != 0)
{
	$player->setObjectiveType(0);
	$entityManager->flush();
}

$page = null;
if (array_key_exists('page',$_GET))
{
	$page = $_GET['page'];
}
$template_name = "";
$title = 'Fleet Command';

$qb = $entityManager->createQueryBuilder();
$qb->select('m')
	->from('Message','m')
	->where('m.recipient = :player and m.read=false')
	->setParameter('player',$player);
$query = $qb->getQuery();
$nbMessagesUnread = count($query->getResult());
$smarty->assign('nb_messages',$nbMessagesUnread);

$qb = $entityManager->createQueryBuilder();
$qb->select('p')
	->from('Planet','p')
	->where('p.freedBy = :player')
	->setParameter('player',$player);
$query = $qb->getQuery();
$nbPlanets = count($query->getResult());
$smarty->assign('nb_planets',$nbPlanets);

$smarty->assign('account_deletion',!is_null($player->getDeletionDate()));

$smarty->assign('under_attack',Helper::underAttack($player));

$sector = $player->getSector();
$alertSector = $sector->getWrecks() > 0 || $sector->isHabitable();
$smarty->assign('alert_sector',$alertSector);

$event = $player->getEvent();

Helper::checkBadges($player);

if ($page != 'account' && $page != 'encyclopedia' && $page != 'messages' && $page != 'message' && $page != 'galaxy' && $page != 'displayplayer')
{
	if ($player->isGameOver())
	{
		$page = 'gameover';
	}
	
	if ($player->isVictorious())
	{
		$page = 'victory';
	}
}
if ($player->isVictorious() && $player->getObjectiveType() == OBJECTIVE_SEARCH_EARTH_ID && !$game->hasFoundEarth())
{
	$game->foundEarth(true);
	$entityManager->flush();
	Builder::broadcastMessage('msg.found.earth',array($player->getName()));
}

if ($game->hasFoundEarth() && $player->getObjectiveType() == OBJECTIVE_SEARCH_EARTH_ID)
{
	$player->setAsVictorious();
	$entityManager->flush();
}

if (!is_null($event) && $page != 'gameover' && $page !='victory' && $page != 'encyclopedia' && $page != 'messages' && $page != 'message')
{
	$page = 'event';
}

//global victory/defeat

if ($game->getStage() == STAGE_HUMAN_VICTORY && $page != 'encyclopedia' && $page != 'messages' && $page != 'message' && $page !='galaxy')
{
	$page = 'victory';
}

if ($game->getStage() == STAGE_DEFEAT && $page != 'encyclopedia' && $page != 'messages' && $page != 'message' && $page !='galaxy')
{
	$page = 'defeat';
}

if (!empty($player->getGlobalAnnouncements()))
{
	$page = 'gannouncement';
}

$cacheId = null;
$generated = false;

$helpcontent = null;

if ($page === 'gameover')
{
	$template_name='gameover';
	$token = Helper::generateCSRFToken();
	$smarty->assign('token',$token);
}
elseif ($page === 'victory')
{
	$template_name='victory';
	include('victory.php');
}
elseif ($page === 'fleet')
{
	$template_name='fleet';
	$helpcontent = $i18n->getText('help.fleet');
	include('fleet.php');
}
elseif ($page === 'sector')
{
	$template_name='sector';
	$helpcontent = $i18n->getText('help.sector');
	include('sector.php');
}
elseif ($page === 'ship')
{
	$template_name='ship';
	include('displayship.php');
}
elseif ($page === 'planet')
{
	$template_name='planet';
	include('planet.php');
}
elseif ($page === 'ptransfer')
{
	$template_name='planetarytransfer';
	include('displayplanetarytransfer.php');
}
elseif ($page === 'encyclopedia')
{
	//$smarty->setCaching(Smarty::CACHING_LIFETIME_SAVED);
	//$smarty->setCacheLifetime(ENCYCLOPEDIA_CACHE_TIME);
	$generated = true;
	$template_name='encyclopedia';
	//$cacheId = 'encyclopedia';
	include('encyclopedia.php');
}
elseif ($page === 'displayplayer')
{
	$template_name = 'displayplayer';
	include('displayplayer.php');
}
elseif ($page === 'characters')
{
	$template_name='characters';
	$helpcontent = $i18n->getText('help.characters');
	include('characters.php');
}
elseif ($page === 'history')
{
	$template_name='history';
	include('history.php');
}
elseif ($page === 'editfleetname')
{
	$template_name='editfleetname';
	include('editfleetname.php');
}
elseif ($page === 'editshipname')
{
	$template_name='editshipname';
	include('editshipname.php');
}
elseif ($page === 'messages')
{
	$template_name='messages';
	include('messages.php');
}
elseif ($page === 'message')
{
	$template_name='message';
	include('message.php');
}
elseif ($page === 'politics')
{
	$template_name='politics';
	$helpcontent = $i18n->getText('help.politics');
	include('politics.php');
}
elseif ($page === 'account')
{
	$template_name='account';
	include('account.php');
}
elseif ($page === 'planets')
{
	$template_name='planets';
	$helpcontent = $i18n->getText('help.planets');
	include('planets.php');
}
elseif ($page === 'transfer')
{
	$template_name='transfer';
	include('transfer.php');
}
elseif ($page === 'galaxy')
{
	$template_name='galaxy';
	$helpcontent = $i18n->getText('help.galaxy');
	include('galaxy.php');
}
elseif ($page === 'event')
{
	$template_name='event';
	$helpcontent = $i18n->getText('help.event');
	include('event.php');
}
elseif ($page === 'gannouncement')
{
	$template_name='gannouncement';
	include('announcement.php');
}
elseif ($page === 'defeat')
{
	$template_name='defeat';
	include('defeat.php');
}
else
{
	$template_name = "dashboard";
	$helpcontent = $i18n->getText('help.dashboard');
	include('dashboard.php');
}

$flashmsg = Tools::getFlashMsg();
$smarty->assign('flashmsg',$flashmsg);
$smarty->assign('hasflashmsg',$flashmsg != '');

$smarty->assign('permdeath',$game->getPermanentDeath());

$smarty->assign('title',$title);
$content = '';

if ($generated)
{
	$filename = __DIR__.'/cache/encyclopedia.html';
	$file = @fopen($filename,'r');
	if (!$file)
	{
		$writefile = fopen($filename,'w');
		fwrite($writefile,$smarty->fetch($template_name.'.tpl'));
		fclose($writefile);
		$file = fopen($filename,'r');
	}
	$content = fread($file,filesize($filename));
	fclose($file);
}
else
{
	$content = $smarty->fetch($template_name.'.tpl');
}

$smarty->assign('content',$content);

// i18n

$smarty->assign('menu_dashboard',$i18n->getText('lbl.menu.dashboard'));
$smarty->assign('menu_messages',$i18n->getText('lbl.menu.messages'));
$smarty->assign('menu_encyclopedia',$i18n->getText('lbl.menu.encyclopedia'));
$smarty->assign('menu_signout',$i18n->getText('lbl.menu.signout'));
$smarty->assign('menu_sector',$i18n->getText('lbl.menu.sector'));
$smarty->assign('menu_fleet',$i18n->getText('lbl.menu.fleet'));
$smarty->assign('menu_history',$i18n->getText('lbl.menu.history'));
$smarty->assign('menu_event',$i18n->getText('lbl.menu.event'));
$smarty->assign('menu_politics',$i18n->getText('lbl.menu.politics'));
$smarty->assign('menu_account',$i18n->getText('lbl.menu.account'));
$smarty->assign('menu_galaxy',$i18n->getText('lbl.menu.galaxy'));
$smarty->assign('menu_planets',$i18n->getText('lbl.menu.planets'));
$smarty->assign('menu_characters',$i18n->getText('lbl.menu.characters'));
$smarty->assign('lbl_permanent_death',$i18n->getText('lbl.permanent.death'));
$smarty->assign('helpcontent',$helpcontent);
$smarty->assign('version',VERSION);
$smarty->display('index.tpl');
