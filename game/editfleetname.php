<?php

$fleetname = $player->getFleet()->getName();

$smarty->assign('fleetname',$fleetname);

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
