<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$fleet = $player->getFleet();

	$sector = $player->getSector();

	$ennemies = $sector->getEnnemies();
    
    $qb = $entityManager->createQueryBuilder();
    $qb->select('p')
        ->from('Planet','p')
        ->where('p.freedBy = :player')
        ->setParameter('player',$player);
    $query = $qb->getQuery();
    $planets = $query->getResult();

	if (count($ennemies) == 0)
	{
		$nbpassengers = $_POST['valnbpassengers'];
		$nbpopulation = $_POST['valnbpopulation'];
		
		$ships = $fleet->getShips();
		$checknbpopulation = 0;
		foreach ($ships as $ship)
		{
			$checknbpopulation += $ship->getPassengers();
		}
        foreach ($planets as $planet)
        {
            $checknbpopulation += $planet->getPopulation();
        }
        
		$values = array_values($nbpassengers);
		foreach ($values as $value)
		{
			$checknbpopulation -= $value;
		}
        
		$values = array_values($nbpopulation);
		foreach ($values as $value)
		{
			$checknbpopulation -= $value;
		}
		if ($checknbpopulation !=0)
		{
			Tools::setFlashMsg($i18n->getText('msg.impossible.action'));
		}
		else
		{
			foreach ($nbpassengers as $shipid => $nb)
			{
				$ship = $entityManager->getRepository('Ship')->find($shipid);
				if ($ship->getFleet()->getId() == $fleet->getId() && $nb <= $ship->getType()->getMaxPassengers())
				{
					$ship->setPassengers($nb);
				}
				else
				{
					throw new Exception('This ship does not belong to this fleet or the maximum passengers has been exceeded');
				}
			}
			
			foreach ($nbpopulation as $planetid => $nb)
			{
				$planet = $entityManager->getRepository('Planet')->find($planetid);
				if ($planet->getFreedBy()->getId() == $player->getId())
				{
					$planet->setPopulation($nb);
				}
				else
				{
					throw new Exception('This planet does not belong to this player');
				}
			}
			$entityManager->flush();
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=planets');
