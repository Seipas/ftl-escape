<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$shipid = $_GET['id'];

	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}

	$ship = $entityManager->getRepository('Ship')->find($shipid);
	$fleet = $player->getFleet();
	if ($ship->getFleet()->getId() == $fleet->getId())
	{
		echo $ship->getName();
		$fleet->protectShip($ship);
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
