<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    if (is_numeric($_GET['id']))
    {
        $nexus = $entityManager->find('Nexus',$_GET['id']);
        if (!$nexus->isAttacked() && $nexus->getStatus() == PLANET_STATUS_OCCUPIED && $nexus->getAttackCount() < PLANET_MAX_ATTACK_PER_DAY)
        {
			$jumped = Builder::jump($player,false,null,$nexus);
			$nexus->attack();
			if(!$jumped)
			{
				Tools::setFlashMsg($i18n->getText('msg.fleet.unable.to.jump'));
			}
        }
        elseif ($nexus->getAttackCount() >= PLANET_MAX_ATTACK_PER_DAY)
        {
            Tools::setFlashMsg($i18n->getText('msg.nexus.max.attack.reached'));
        }
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location:index.php');

$entityManager->flush();