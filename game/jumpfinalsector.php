<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$game = $entityManager->getRepository('Game')->find(1);
	if ($game->getStage() == STAGE_COUNTER_ATTACK_TWO)
	{
		$username = $_SESSION['username'];
		$player = $entityManager->getRepository('Player')->findOneByLogin($username);
		$lvl = $player->getEnemyColonyLevel();
		$i18n = new I18n();
		$i18n->autoSetLang();
		if (count($player->getSector()->getEnnemies()) == 0)
		{
			if (Helper::hasEvent($player))
			{
				Tools::setFlashMsg($i18n->getText('msg.cannot.jump.while.event'));
			}
			elseif ($lvl == FINAL_WIN_LEVEL)
			{
				// WIN !
				$player->setAsVictorious();
				$entityManager->flush();
			}
			else
			{
				Builder::jump($player,false,null,null,true);
				$player->setEnemyColonyLevel($lvl+1);
				$entityManager->flush();
			}
		}
		else
		{
			// well... hack ?
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
