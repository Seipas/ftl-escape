<?php

$announcements = $player->getGlobalAnnouncements();
$announcement = array_shift($announcements);

$data = array('playername'=>$player->getLogin(),'nbships'=>count($player->getFleet()->getShips()));

$smarty->assign('txt',$i18n->getText('txt.announcement.'.$announcement,$data));

//I18N
$smarty->assign('lbl_announcement',$i18n->getText('lbl.announcement'));

$player->setGlobalAnnouncements($announcements);
$entityManager->flush();