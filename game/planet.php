<?php

$id = $_GET['id'];
$fleet = $player->getFleet();
if (is_numeric($id))
{
    $planet = $entityManager->find('Planet',$id);
    if (!is_null($planet->getFreedBy()) && $planet->getFreedBy()->getId() == $player->getId())
    {
        $smarty->assign('planet',$planet);
    }
    
    $qb = $entityManager->createQueryBuilder();
    $equipments = $qb->select('t')
                    ->from('PlanetaryEquipmentType','t')
                    ->where('t.buildable = true')
                    ->andWhere('t.price <= :material')
                    ->setParameter('material',$fleet->getMaterial())
                    ->getQuery()
                    ->getResult();
    $smarty->assign('available_equipments',$equipments);
    
}

// I18n
$smarty->assign('i18n',$i18n);

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);