<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$i18n = new I18n();
$i18n->autoSetLang();
$game = $entityManager->getRepository('Game')->find(1);
if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	if ($_POST['cpassword']!='')
	{
		if (check_auth($username,$_POST['cpassword']) && $_POST['npassword'] != '')
		{
			if ($_POST['npassword'] === $_POST['rpassword'])
			{
				$player->setPassword($_POST['npassword']);
				Tools::setFlashMsg($i18n->getText('msg.password.changed'));
			}
			else
			{
				Tools::setFlashMsg($i18n->getText('msg.passwords.mismatch'));
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.invalid.password'));
		}
	}

	if ($_POST['email'] != '' && filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
	{
		$player->setEmail($_POST['email']);
		Tools::setFlashMsg($i18n->getText('msg.email.changed'));
	}
	
	$player->setShowAsTable(!is_null($_POST['showtables']));
	
	if ($game->getChooseDifficulty())
	{
		if(is_numeric($_POST['difficulty']) && $_POST['difficulty'] <= MAX_DIFFICULTY)
		{
			$difficulty = $_POST['difficulty'];
			if ($difficulty <= 0)
			{
				$difficulty = null;
			}
			$player->setDifficulty($difficulty);
		}
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=account');
