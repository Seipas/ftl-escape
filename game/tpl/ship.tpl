{if !$not_in_ships}
<h1>{$name} <a href="index.php?page=editshipname&id={$id}"><img src="img/edit.png"/></a></h1>
{$name="ship.battlecruiser.name"}
<div class="ship_details">
    <div class="ship_general">
        {$red=0}
        {$green=0}
        {$blue=0}
        {if $heavydamages}
            {$red=255}
        {elseif $damages > 0}
            {$red=255}
            {$green=162}
        {/if}
        <img style="display:none;" id="canvasSource" src="img/ship/200x400/{$type}.png"/>
        <canvas id="area" width="200" height="400">
        </canvas>
        
        <script type="text/javascript">
            window.onload = function() {
            var canvas = document.getElementById("area");
            var context = canvas.getContext("2d");
            var image = document.getElementById("canvasSource");
            context.drawImage(image, 0, 0);
            var imgd = context.getImageData(0, 0, 200, 400);
            var pix = imgd.data;
            for (var i = 0, n = pix.length; i < n; i += 4) {
                pix[i  ] = {$red}; 	// red
                pix[i+1] = {$green}; 	// green
                pix[i+2] = 0; 	// blue
            }
            context.putImageData(imgd, 0, 0);
            };
        </script>
    </div>
    {if !$survivesystems}
    <div class="ship_o2 blinking">
        <img src="img/ship/200x400/{$type}.o2.png"/>
    </div>
    {/if}
    {if !$ftl}
    <div class="ship_ftl blinking">
        <img src="img/ship/200x400/{$type}.ftl.png"/>
    </div>
    {/if}
    <div class="clear"></div>
    <p>{$lbl_hp} : {$hp}/{$maxhp}</p>
</div>
<div class="ship_info">
    <dl>
    <dt>{$lbl_class}</dt><dd>{$type_name} Mk. {$level}</dd>
    <dt>{$lbl_staff}</dt><dd>{$staff} <progress value="{$staff}" max="{$maxstaff}"></progress> {$maxstaff}</dd>
    <dt>{$lbl_passengers}</dt><dd>{$passengers} <progress value="{$passengers}" max="{$maxpassengers}"></progress> {$maxpassengers}</dd>
    <dt><img src="img/system.attack.png"/> {$lbl_attack}</dt><dd>{$attack} (<a class="tooltip">{$type_attack}<span class="classic">{$lbl_normal_type_value}</span></a>)</dd>
    <dt><img src="img/system.defense.png"/> {$lbl_defense}</dt><dd>{$defense} (<a class="tooltip">{$type_defense}<span class="classic">{$lbl_normal_type_value}</span></a>)</dd>
    </dl>
    <p class="ship_compo {if $ftl}ship_compo_green{else}ship_compo_red blinking{/if}">FTL<br /><span class="ship_compo_info"><progress value="{if $ftl}{$jumpstatus}{else}0{/if}" max="{$maxjumpstatus}"></progress></span></p>
    <p class="ship_compo {if $survivesystems}ship_compo_green{else}ship_compo_red blinking{/if}">O2<br /><span class="ship_compo_info">{$oxygenlevel}%</span></p>
</div>
{else}
<span style="font-weight: bold; font-size: 30px;">?</span>
{/if}