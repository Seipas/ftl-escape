<h1>{$lbl_characters}</h1>
<ul id="characters">
    {foreach $characters as $character}
    <li>
        <p><img src="img/avatar/64x64/{$character->getAvatar()}.png"/></p>
        <p>{$character->getName()} {if $chief_id == $character->getId()}<img src="img/star.png" alt="star"/>{/if} {if $can_assassinate && $chief_id != $character->getId()} <a href="assassinate.php?id={$character->getId()}&token={$token}"><img src="img/assassinate.png"/> {$lbl_assassinate}</a>{/if} {if $can_elect}{if is_null($faked_elected) || $faked_elected->getId() != $character->getId()}<a href="fakeelection.php?id={$character->getId()}&token={$token}"><img src="img/system.election.png" />{$lbl_fake_elections}</a>{/if}{/if}</p>
        <p>{$lbl_type} : {$i18n->getText($character->getType())}</p>
        <!--
        @TODO : check if this representation is OK
        <p>
            <img src="img/male.png" alt="male gender"/><progress value="{$character->getGender()}" max="100"></progress><img src="img/female.png" alt="female gender"/>
        </p>-->
        <p>
            {$lbl_ship} : {$character->getShip()->getName()} ({$i18n->getText($character->getShip()->getType()->getName())})
            {$currentShipId = $character->getShip()->getId()}
            {if $can_act}
            <form action="transfercharacter.php" method="post">
                <input type="hidden" name="token" value="{$token}"/>
                <input type="hidden" name="characterid" value="{$character->getId()}"/>
                <select name="ship">
                    {foreach $ships as $ship}
                    {if $ship['id'] != $currentShipId}
                        <option value="{$ship['id']}">{$ship['name']} ({$ship['type']})</option>
                    {/if}
                    {/foreach}
                </select>
                <input type="submit" value="{$lbl_transfer}"/>
            </form>
            {/if}
        </p>
        {if $character->getType() == 'military'}
        <p><a class="character_item tooltip"><span class="classic">{$lbl_character_attack}</span><img src="img/system.attack.png"/> <progress value="{$character->getAttackBonus() * 100}" max="100"></progress>{$character->getAttackBonus() * 100}%</a></p>
        <p><a class="character_item tooltip"><span class="classic">{$lbl_character_defense}</span><img src="img/system.defense.png"/> <progress value="{$character->getDefenseBonus() * 100}" max="100"></progress>{$character->getDefenseBonus() * 100}%</a></p>
        {/if}
        <p><a class="character_item tooltip"><span class="classic">{$lbl_character_moral}</span><img src="img/system.moral.png"/> <progress value="{$character->getMoralBonus() * 100}" max="100"></progress>{$character->getMoralBonus() * 100}%</a></p>
        <p><a class="character_item tooltip"><span class="classic">{$lbl_character_popularity}</span><progress value="{$character->getPopularity()}" max="100"></progress>{$character->getPopularity()}%</a></p>
        <p>{$lbl_politics} : <a class="tooltip"><span class="classic">{$i18n->getText($character->getPoliticalSystem()->getDefinition())}</span>{$i18n->getText($character->getPoliticalSystem()->getName())}</a></p>
    </li>
    {/foreach}
</ul>
