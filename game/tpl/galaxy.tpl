{if $game_level == 2}
<h1>{$lbl_colony}</h1>
<div class="datagrid">
<table id="tbl_enemy_colony">
	<thead><tr><th>{$lbl_player}</th><th>Protection Extérieure</th><th>Protection Intérieure</th><th>Stabilisateur</th></tr></thead>
<tbody>
{foreach $players as $player}
{if $player->getEnemyColonyLevel() > 0}
<tr>
	<td>{if count($player->getFleet()->getShips()) == 0}<img src="img/assassinate.png"/> {/if}{$player->getLogin()}{if $player->getEnemyColonyLevel() == 3 && count($player->getSector()->getEnnemies()) ==0} <img src="img/star.png"/>{/if}</td>
	{$cpt=1}
	{while $cpt <= 3}
		{if $cpt < $player->getEnemyColonyLevel()}<td class="enc_green"></td>
		{elseif $cpt == $player->getEnemyColonyLevel()}
			{if count($player->getSector()->getEnnemies()) == 0}<td class="enc_green"><img src="img/star.png"/></td>
			{elseif count($player->getFleet()->getShips()) == 0}<td class="enc_dark_grey"><img src="img/assassinate.png"/></td>
			{else}<td class="enc_orange"><img src="img/system.attack.png"/></td>{/if}
		{else}<td class="enc_red"></td>{/if}
		{$cpt=$cpt+1}
	{/while}
</tr>
{/if}
{/foreach}
</tbody>
</table>
</div>
{/if}

{if $game_level == 1}
<h1>{$lbl_nexus}</h1>
<ul id="gplanets">
	{foreach $nexii as $nexus}
	<li class="pstatus{$nexus->getStatus()}">
		<div class="nexus">
			<span class="title">{$nexus->getName()}</span><br />
			{if $nexus->getStatus() == 4}
				{$lbl_destroyed_by} : {$nexus->getDestroyedBy()->getName()}<br />
			{/if}
			{if $nexus->getStatus() == 2}
				{$lbl_attacked_by} : {$nexus->getSector()->getPlayer()->getName()}<br />
			{/if}
		</div>
	</li>
	{/foreach}
</ul>
<div style="clear:both;"></div>
{/if}

{$nb_survivors=0}

<h1>{$lbl_planets}</h1>
<ul id="gplanets">
{foreach $planets as $planet}
<li class="pstatus{$planet->getStatus()}">
	<div class="planet">
		<span class="title">{$planet->getName()}</span><br />
		{if $planet->getStatus() == 0}
			{$nb_survivors = $nb_survivors + $planet->getPopulation()}
			{$lbl_freed_by} : {$planet->getFreedBy()->getName()}<br />
		{/if}
		{if $planet->isAttacked() && $planet->getStatus() == 2}
			{$lbl_attacked_by} : {$planet->getSector()->getPlayer()->getName()}<br />
		{/if}
	</div>
</li>
{/foreach}
{foreach $new_planets as $planet}
<li class="pstatuscolonized">
	<div class="planet">
		<span class="title">{$planet->getName()}</span><br />
		{if $planet->getStatus() == 0}
			{$nb_survivors = $nb_survivors + $planet->getPopulation()}
			{$lbl_colonized_by} : {$planet->getFreedBy()->getName()}<br />
		{/if}
		{if $planet->isAttacked() && $planet->getStatus() == 2}
			{$lbl_attacked_by} : {$planet->getSector()->getPlayer()->getName()}<br />
		{/if}
	</div>
</li>
{/foreach}
</ul>
<div style="clear:both;"></div>
<h1>{$lbl_galaxy}</h1>
<div class="datagrid">
<table id="tbl_galaxy">
<thead><tr><th>{$lbl_player}</th><th>{$lbl_nb_ships}</th><th>{$lbl_nb_survivors}</th></tr></thead>
<tbody>
	{$nb_ships=0}
	
	{foreach $players as $player}
	{if !$player->isDeleted()}
		{$nbs = count($player->getFleet()->getShips())}
		{$nbv = Helper::calculateSurvivors($player)}
		{$nb_ships = $nb_ships + $nbs}
		{$nb_survivors = $nb_survivors + $nbv}
		<tr><td><a href="index.php?page=displayplayer&id={$player->getId()}">{$player->getLogin()}</a></td><td>{$nbs}</td><td>{$nbv}</td></tr>
	{/if}
	{/foreach}
</tbody>
<tfoot><tr><td></td><td>{$nb_ships}</td><td>{$nb_survivors}</td></tr></tfoot>
</table>
</div>
<script type="text/javascript">
sortable_table("tbl_galaxy");
</script>
