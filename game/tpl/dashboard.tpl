<div id="dashboard_fleet">
	<h1>Notre flotte</h1>
	{if $showtable}
	<div class="datagrid">
	<table id="tbl_hs">
		<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_hp}</th></tr></thead>
		<tbody>
					{$macro_warning = $jump_status_num < $safe_jump_num}
			{$nbFTLpbs = 0}
			{$nbSSpbs = 0}
			{$nbStaffpbs = 0}
			{$nbdamaged = 0}
	{foreach $ships as $ship}
		{$enoughStaff = $ship->getStaff() != 0}
		{$FTLDrive = $ship->getFTLDrive()}
		{$surviveSystems = $ship->getSurviveSystems()}
		{$oxygen = $ship->getOxygenLevel()}
		{$alert = !$ship->canJump() || !$FTLDrive || !$surviveSystems}
		{$macro_warning = $macro_warning || !$ship->canJump() || !$FTLDrive}
		{$id=$ship->getId()}

		{$type=$ship->getType()->getName()}
			<tr {if $alert}class="alert"{/if}>
		<td>{$ship->getName()}
			{if $alert}
				{if !$enoughStaff}<a class="tooltip alert_staff"><img src="img/alert_staff.png" /><span class="classic">{$msg_not_enough_staff}</span></a>{/if}
				{if !$FTLDrive}<a class="tooltip alert_ftl"><img src="img/alert_ftl.png" /><span class="classic">{$lbl_alert_ftl}</span></a>{/if}
				{if !$surviveSystems}<a class="tooltip alert_survive"><img src="img/alert_survive.png" /><span class="classic">{$lbl_oxygen_level} : {$oxygen}%</span></a>{/if}
			{/if}
		</td>
		<td>{$i18n->getText($type)} (Mk. {Tools::roman_numerals($ship->getLevel() + 1)})</td>
		<td {if $ship->getHP() < $ship->getMaxHP()}class="important"{/if}>{$ship->getHP()}/{$ship->getMaxHP()}</td>
	</tr>
	{/foreach}
	</tbody>
	</table>
	</div>
	{else}
	<script type="text/javascript">
	function displaymonitor(id)
	{
		monitor = document.getElementById('monitor');
		info = document.getElementById('info_ship'+id);
		monitor.innerHTML = info.innerHTML;
	}
	function resetmonitor()
	{
		monitor = document.getElementById('monitor');
		info = document.getElementById('monitor_init');
		monitor.innerHTML = info.innerHTML;
	}
	</script>

	<div id="display_fleet">
		<div id="monitor-bg" class="monitor-bg"><div id="monitor" class="monitor"></div></div>
			{$macro_warning = $jump_status_num < $safe_jump_num}
			{$nbFTLpbs = 0}
			{$nbSSpbs = 0}
			{$nbStaffpbs = 0}
			{$nbdamaged = 0}
	{foreach $ships as $ship}
		{$enoughStaff = $ship->getStaff() != 0}
		{$FTLDrive = $ship->getFTLDrive()}
		{$surviveSystems = $ship->getSurviveSystems()}
		{$oxygen = $ship->getOxygenLevel()}
		{$alert = !$ship->canJump() || !$FTLDrive || !$surviveSystems}
		{$macro_warning = $macro_warning || !$ship->canJump() || !$FTLDrive}
		{$id=$ship->getId()}

		{$type=$ship->getType()->getName()}
		

		<div class="dashboard_ship">
		<div class="ship_general" id="ship{$id}">
			
			<img style="display:none;" id="canvasSource{$id}" src="img/ship/50x100/{$type}.png"/>
			<a class="info_monitor" onmouseover="displaymonitor({$id});" onmouseout="resetmonitor()">
			<canvas id="area{$id}" width="50" height="100">
			</canvas>
			<span style="display:none;" id="info_ship{$id}">
				<strong>{$ship->getName()}</strong><br/>
				{$i18n->getText($ship->getType()->getName())}<br/>
				<progress value="{$ship->getHP()}" max="{$ship->getMaxHP()}"></progress> {$ship->getMaxHP()}<br/>
				{if !$surviveSystems}
				<img src="img/alert_survive.png" /> {$lbl_oxygen_level} : {$oxygen}%<br />
				{/if}
				{if !$FTLDrive}
				<img src="img/alert_ftl.png" /> {$lbl_alert_ftl}<br />
				{/if}
				{if !$enoughStaff}
				<img src="img/alert_staff.png" /> {$msg_not_enough_staff}<br />
				{/if}
			</span>
			</a>
		 </div>
		{if !$surviveSystems}
		{$nbSSpbs=$nbSSpbs+1}
		<div class="ship_o2 blinking" onmouseover="displaymonitor({$id});" onmouseout="resetmonitor()">
			<img src="img/ship/50x100/{$type}.o2.png"/>
		</div>
		{/if}
		{if !$FTLDrive}
		{$nbFTLpbs=$nbFTLpbs+1}
		<div class="ship_ftl blinking" onmouseover="displaymonitor({$id});" onmouseout="resetmonitor()">
			<img src="img/ship/50x100/{$type}.ftl.png"/>
		</div>
		{/if}
		{if !$enoughStaff}
		{$nbStaffpbs=$nbStaffpbs+1}
		{/if}
		</div>
	{/foreach}
	<div class="clear"></div>

	<script type="text/javascript">
				window.onload = function() {
					{foreach $ships as $ship}
					{$id=$ship->getId()}
					{$damages = $ship->getMaxHP() - $ship->getHP()}
		{$heavydamages=$ship->getHP() < $ship->getMaxHP() / 2}
		{$red=0}
			{$green=0}
			{$blue=0}
			{if $heavydamages}
				{$red=255}
				{$nbdamaged=$nbdamaged+1}
			{elseif $damages > 0}
				{$nbdamaged=$nbdamaged+1}
				{$red=255}
				{$green=162}
			{/if}
				var canvas = document.getElementById("area{$id}");
				var context = canvas.getContext("2d");
				var image = document.getElementById("canvasSource{$id}");
				context.drawImage(image, 0, 0);
				var imgd = context.getImageData(0, 0, 50, 100);
				var pix = imgd.data;
				for (var i = 0, n = pix.length; i < n; i += 4) {
					pix[i  ] = {$red}; 	// red
					pix[i+1] = {$green}; 	// green
					pix[i+2] = 0; 	// blue
				}
				context.putImageData(imgd, 0, 0);
				{/foreach}
				};
			</script>
	<div id="monitor_init" style="display:none;">
	{if $nbdamaged > 0}{$lbl_alert_damaged_ships} : {$nbdamaged}<br />{/if}
	{if $nbSSpbs > 0}<a class="tooltip"><img src="img/alert_survive.png" /><span class="classic">{$lbl_oxygen_level}</span></a> : {$nbSSpbs}<br />{/if}
	{if $nbFTLpbs > 0}<a class="tooltip"><img src="img/alert_ftl.png" /><span class="classic">{$lbl_alert_ftl}</span></a> : {$nbFTLpbs}<br />{/if}
	{if $nbStaffpbs > 0}<a class="tooltip"><img src="img/alert_staff.png" /><span class="classic">{$msg_not_enough_staff}</span></a> : {$nbStaffpbs}{/if}

	{if $nbdamaged==0 && $nbSSpbs == 0 && $nbFTLpbs == 0 && $nbStaffpbs == 0}
	{$lbl_RAS}
	{/if}
	</div>
	</div>

	{/if}

</div>

{if $sector_nb_ennemies > 0}
<div id="dashboard_ennemies">
	<h1>Ennemis repérés !</h1>
	{if $showtable}
	<div class="datagrid">
	<form action="attack.php" method="post">
		<input type="hidden" name="token" value="{$token}"/>
	<table id="tbl_es">
		<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_hp}</th><th {if $sector_nb_ennemies > 1}colspan=2{/if}>{$lbl_attack_type}</th></tr></thead>
		<tfoot><tr><td></td><td></td><td></td><td {if $sector_nb_ennemies > 1}colspan=2{/if}><input type="submit" value="{$lbl_attack}"/></td></tr></tfoot>
		<tbody>
	{foreach $ennemies as $ennemy name="ennemies"}
	<tr><td>{$ennemy->getName()}</td><td>{$i18n->getText($ennemy->getType()->getName())}</td><td>{$ennemy->getHP()}/{$ennemy->getType()->getMaxHP()}</td><td><input type="radio" name="ennemy" value="{$ennemy->getId()}" id="radio{$ennemy->getId()}"/><label for="radio{$ennemy->getId()}">{$lbl_concentrate_fire} ({$combined_attack_value})</label></td>{if $smarty.foreach.ennemies.first && $sector_nb_ennemies > 1}<td rowspan="{$sector_nb_ennemies}" class="nohover"><input type="radio" name="ennemy" id="radiodf" value="-1" /><label for="radiodf">{$lbl_distributed_fire} ({$distributed_attack_value})</label></td>{/if}</tr>
	{/foreach}
	</tbody>
	</table>
	</form>
	
	</div>
	{else}
	<div class="dashboard_eships">
		<script type="text/javascript">
	function displayemonitor(id)
	{
		monitor = document.getElementById('emonitor');
		info = document.getElementById('info_eship'+id);
		monitor.innerHTML = info.innerHTML;
	}
	function resetemonitor()
	{
		monitor = document.getElementById('emonitor');
		//info = document.getElementById('monitor_init');
		monitor.innerHTML = "";
	}
	</script>
		<div class="monitor-bg"><div id="emonitor" class="monitor"></div></div>
		<form action="attack.php" method="post">
		<input type="hidden" name="token" value="{$token}"/>
		{$lbl_concentrate_fire} : {$combined_attack_value}
		<div class="clear"></div>
	{foreach $ennemies as $ennemy name="ennemies"}
	{$id = $ennemy->getId()}
	{$damaged = $ennemy->getHp() < $ennemy->getType()->getMaxHP()}
	{$heavydamaged = $ennemy->getHp() < $ennemy->getType()->getMaxHP() / 2}
	<div class="eship {if $heavydamaged}heavydamaged{elseif $damaged}damaged{/if}" id="eship{$id}" onmouseover="displayemonitor({$id});" onmouseout="resetemonitor();">
		<label for="radio{$ennemy->getId()}">
		<span style="display:none" id="info_eship{$id}">
		<strong>{$ennemy->getName()}</strong><br/>
		{$i18n->getText($ennemy->getType()->getName())}<br/>
		<progress value="{$ennemy->getHP()}" max="{$ennemy->getType()->getMaxHP()}"></progress>
		</span>
		<input type="radio" name="ennemy" value="{$ennemy->getId()}" id="radio{$ennemy->getId()}"/>
		<img src="img/eship/50x100/{$ennemy->getType()->getName()}.png"/>
		</label>
	</div>
	{/foreach}
	<div class="clear"></div>
	{if $sector_nb_ennemies > 1}<input type="radio" name="ennemy" id="radiodf" value="-1" /><label for="radiodf">{$lbl_distributed_fire} : {$distributed_attack_value}</label>{/if}
	<p></p><input type="submit" value="{$lbl_attack}"/></p>
	</form>
	</div>
	{/if}
	<form action="usenuclearweapon.php" method="post">
		<input type="hidden" name="token" value="{$token}"/>
		{$lbl_nuclear_weapons} : {$nuclear_weapons}/{$max_nuclear_weapons} {if $nuclear_weapons > 0}<input type="submit" value="{$lbl_use_nuclear_weapon}"/>{/if}
	</form>
</div>
{/if}

<div id="dashboard_jump" class="{if $jump_status_num <= 8 && $jump_status_num >= 6}jump_preparing{elseif $jump_status_num <= 10 && $jump_status_num >= 9}jump_safe{else}jump_notready{/if}">
	<h1>{$lbl_ftljump}</h1>
	{if $sector_nb_ennemies == 0 && !$is_enemy_colony}
		<a href="preparejump.php?token={$token}">{$lbl_prepare_jump}</a>
		{if $jump_status_num}
			/ <a href="canceljumppreparation.php?token={$token}">{$lbl_cancel_jump}</a>
		{/if}
	{/if}

	{if $jump_status_num != $FTLbroken && $enough_fuel && !$is_enemy_colony}
	<div class="center">
	<table id="jump_level_dashboard" class="jump_level">
		<caption><p id="jump_level_title">{$lbl_jump_status}</p><p id="jump_level_status_text">{$jump_status}</p></caption>
	<tr><td class="{if $jump_status_num > 10}jl_green{elseif $jump_status_num == 10}jl_current{else}jl_red{/if}">{$jump_levels[10]}</td></tr>
	<tr><td class="{if $jump_status_num > 9}jl_green{elseif $jump_status_num <= 9 && $jump_status_num >= 7}jl_current{else}jl_red{/if}">
		{$jump_status_sync}
		{if $jump_status_num <= 9 && $jump_status_num >= 7}
		<div class="mini_load">
			{if $jump_status_num >= 7}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 7}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 8}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 9}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		</div>
		{/if}
		</td></tr>
	<tr><td class="{if $jump_status_num > 6}jl_green{elseif $jump_status_num <= 6 && $jump_status_num >= 3}jl_current{else}jl_red{/if}">
		{$jump_status_warming}
		{if $jump_status_num <= 6 && $jump_status_num >= 3}
		<div class="mini_load">
			{if $jump_status_num >= 3}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 4}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 5}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
			{if $jump_status_num >= 6}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		</div>
		{/if}
		</td></tr>
	<tr><td class="{if $jump_status_num > 2}jl_green{elseif $jump_status_num == 2}jl_current{else}jl_red{/if}">{$jump_levels[2]}</td></tr>
	<tr><td class="{if $jump_status_num > 1}jl_green{elseif $jump_status_num == 1}jl_current{else}jl_red{/if}">{$jump_levels[1]}</td></tr>
	<tr><td class="{if $jump_status_num > 0}jl_green{elseif $jump_status_num == 0}jl_current{else}jl_red{/if}">{$jump_levels[0]}</td></tr>
	</table>

	{if $jump_status_num >= $min_jumpable_status }
	<form id="jumpform" name="jumpform" action="jump.php" method="post">
		<input type="hidden" name="token" value="{$token}"/>
	{/if}
	<input
	   type="submit"
	   name="jumpbtn"
	   class="jumpbutton{if $jump_status_num < $min_jumpable_status}disabled{/if}"
	   value="{$lbl_jump}"
	   onMouseOver="goLite(this.form.name,this.name)"
	   onMouseOut="goDim(this.form.name,this.name)"
	   {if $macro_warning}onClick="return overlay();"{/if}>
	{if $jump_status_num >= $min_jumpable_status}
	</form>
	{/if}
	</div>
	{elseif !$enough_fuel}
	{$msg_not_enough_fuel}
	{else}
	{if !$is_enemy_colony}
		{$jump_levels[-1]}
	{/if}
	{/if}

	{if $is_enemy_colony && $sector_nb_ennemies == 0}
	<a href="jumpfinalsector.php?token={$token}">Secteur suivant ?</a>
	{/if}

</div>
<script type="text/javascript">
{if $sector_nb_ennemies > 0}
sortable_table("tbl_es");
{/if}
</script>

<div id="overlay">
	<div id="jump_alert">
		<h1>{$lbl_jump_alert}</h1>
		<p>{$msg_alert}</p>
		<p><a onclick="return confirmJump()">{$lbl_confirm}</a> <a onclick="return overlay()">{$lbl_cancel}</a></p>
	</div>
</div>

<script type="text/javascript">
function overlay() {
    el = document.getElementById("overlay");
   el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
   return false;
 }
function confirmJump()
{
	document.getElementById("jumpform").submit();
}
</script>
<script type="text/javascript">
resetmonitor();
</script>
