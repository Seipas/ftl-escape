{if $player}
<h1>{$player->getLogin()}</h1>
<div id="badges">
	<ul>
	{foreach $badges as $badge}
	<li><a class="tooltip"><img src="img/{$badge->getIcon()}.png"/><span class="classic"><p class="badge_title">{$i18n->getText($badge->getName())}</p><p class="badge_description">{$i18n->getText($badge->getDescription())}</p></span></a></li>
	{/foreach}
	</ul>
</div>
<div id="playerinfo">
    {$fleet = $player->getFleet()}
    {$lbl_fleet} : {$fleet->getName()} ({count($fleet->getShips())} {$lbl_ships})<br />
    {$lbl_political_system} : {if !empty($political_system)}{$i18n->getText($political_system->getName())}{else}N/A{/if}
    <ul id="fleet">
        {$ships = $fleet->getShips()}
        {foreach $ships as $ship}
        <li>{$ship->getName()} ({$i18n->getText($ship->getType()->getName())} Mk.{Tools::roman_numerals($ship->getLevel() + 1)})</li>
        {/foreach}
    </ul>
</div>
{else}
{$msg_is_deleted}
{/if}
