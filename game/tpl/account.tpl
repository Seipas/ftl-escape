<h1>{$lbl_badges}</h1>
<div id="badges">
	<ul>
	{foreach $badges as $badge}
	<li><a class="tooltip"><img src="img/{$badge->getIcon()}.png"/><span class="classic"><p class="badge_title">{$i18n->getText($badge->getName())}</p><p class="badge_description">{$i18n->getText($badge->getDescription())}</p></span></a></li>
	{/foreach}
	</ul>
</div>

<h1>{$lbl_account}</h1>
<div>
<form action="modifyaccount.php" method="post">
	{$lbl_difficulty} :
	<select name="difficulty" {if !$can_modify_difficulty}disabled{/if}>
	<option value="-1" {if $calculated_difficulty}selected="selected"{/if}>{$lbl_calculated_difficulty} ({$i18n->getText("lbl.difficulty.$difficulty")})</option>
	{for $i=1 to $max_difficulty}
	<option value="{$i}" {if $i == $difficulty && !$calculated_difficulty}selected="selected"{/if}>{$i18n->getText("lbl.difficulty.$i")}</option>
	{/for}
	</select>
	<input type="hidden" name="token" value="{$token}"/>
<dl>
	<dt>{$lbl_login}</dt>
	<dd>{$login}</dd>
	<dt>{$lbl_email}</dt>
	<dd><input type="email" name="email" value="{$email}" /></dd>
	<dt>{$lbl_current_password}</dt>
	<dd><input type="password" name="cpassword" /></dd>
	<dt>{$lbl_password}</dt>
	<dd><input type="password" name="npassword" /></dd>
	<dt>{$lbl_password_retype}</dt>
	<dd><input type="password" name="rpassword" /></dd>
	<dt>{$lbl_show_tables}</dt>
	<dd><input type="checkbox" name="showtables" {if $showtables}checked="checked"{/if}/></dd>
</dl>
<input type="submit" value="{$lbl_save}" />
</form>
</div>

{if !$perm_death}
<h1>{$lbl_restart}</h1>
<form action="restartgame.php" id="restartform" method="post">
	<input type="hidden" name="token" value="{$token}"/>
<input type="checkbox" name="rescheck1" />
<input type="checkbox" name="rescheck2" />
<input type="checkbox" name="rescheck3" />
<input type="submit" value="{$lbl_restart}" onClick="return overlay();"/>
</form>
{/if}

<h1>{$lbl_delete}</h1>
{if $account_in_deletion}
{$lbl_account_in_deletion} : {$deletion_date}<br />
<form action="canceldeletion.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
<input type="submit" value="{$lbl_cancel}" />
</form>
{else}
<form action="deleteaccount.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
<input type="checkbox" name="delcheck1" />
<input type="checkbox" name="delcheck2" />
<input type="checkbox" name="delcheck3" />
<input type="submit" value="{$lbl_delete}" />
</form>
{/if}

<div id="overlay">
	<div id="restart_alert">
		<h1>{$lbl_restart_alert}</h1>
		<p>{$msg_alert}</p>
		<p><a onclick="return confirmRestart()">{$lbl_confirm}</a> <a onclick="return overlay()">{$lbl_cancel}</a></p>
	</div>
</div>

<script type="text/javascript">
function overlay() {
    el = document.getElementById("overlay");
   el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
   return false;
 }
function confirmRestart()
{
	document.getElementById("restartform").submit();
}
</script>
