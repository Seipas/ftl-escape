<h1>{$lbl_objective}</h1>
{if $game_level == 1}
	<ul id="planets">
	{foreach $nexii as $nexus}
	<li class="pstatus{$nexus->getStatus()}">
		<div class="nexus">
			<span class="title">{$nexus->getName()}</span><br />
			{if $nexus->getStatus() == 4}
				{$lbl_destroyed_by} : {$nexus->getDestroyedBy()->getName()}<br />
			{/if}
			{if $nexus->getStatus() == 1 && $can_act && $nexus->getAttackCount() < $max_attacks}
				{if $can_jump}
				<a href="attacknexus.php?id={$nexus->getId()}&token={$token}">{$lbl_attack}</a>
				{/if}
				<br />
			{/if}
			{if $nexus->getStatus() == 2}
				{$lbl_attacked_by} : {$nexus->getSector()->getPlayer()->getName()}<br />
			{/if}
		</div>
	</li>
	{/foreach}
	</ul>
{else}
	{if $objective_type == 0}
		<form action="changeobjective.php" method="post">
			<input type="hidden" name="token" value="{$token}"/>
		<input type="radio" name="objective" id="o1" value="1" /><label for="o1">{$lbl_search_habitable} ({$lbl_search_habitable_difficulty}) ({$lbl_search_habitable_bonus})</label><br />
		{if !$earth_found}<input type="radio" name="objective" id="o2" value="2" /><label for="o2">{$lbl_search_earth} ({$lbl_search_earth_difficulty}) ({$lbl_search_earth_bonus})</label><br />{/if}
		{if $game_level < 1}<input type="radio" name="objective" id="o3" value="3" /><label for="o3">Reconquérir les colonies (difficulté cauchemardesque) (moral +50, moral -10/j, attaque +15%)</label><br />{/if}
		<input type="submit" value="{$lbl_confirm}"/>
		</form>
	{else}
		{if $objective_type == 1}
			{$lbl_search_habitable} ({$lbl_search_habitable_bonus})
		{elseif $objective_type == 2}
			{$lbl_search_earth} ({$lbl_search_earth_bonus})<br />
			{$lbl_earth_relics} : {$nb_clues} / {$max_clues}<br />
			{if $nb_clues == $max_clues}
				{$msg_found_way_to_earth} <a href="jumptoearth.php?token={$token}">{$lbl_jump_to_earth}</a>
			{/if}
		{elseif $objective_type == 3}
			<ul id="planets">
			{foreach $planets as $planet}
			<li class="pstatus{$planet->getStatus()}">
				<div class="planet">
					<span class="title">{$planet->getName()}</span><br />
					<a href="#" class="tooltip">{$planet->getMaterial()} ({$planet->getMaterialProduction()})<span class="classic">{$lbl_planet_materials}</span></a><br />
					{if $planet->getStatus() == 0}
						{$lbl_freed_by} : {if $planet->getFreedBy()}{$planet->getFreedBy()->getName()}{else}{$lbl_unknown}{/if}<br />
					{/if}
					{if $planet->getStatus() == 1 && $can_act && $planet->getAttackCount() < $max_attacks}
						{if $can_jump}
						<a href="attackplanet.php?id={$planet->getId()}&token={$token}">{$lbl_attack}</a>
						{/if}
						{if $can_scout}
						- <a href="scoutplanet.php?id={$planet->getId()}&token={$token}">{$lbl_scout}</a>
						{/if}
						<br />
					{/if}
					{if $planet->getStatus() == 2}
						{$lbl_attacked_by} : {$planet->getSector()->getPlayer()->getName()}<br />
					{/if}
					{if $planet->getStatus() != 0}
						{$lbl_known_forces} : {$garrison = $planet->getKnownGarrison()}
						{if !empty($garrison)}
							<br />{date('d/m/y H:i:s',$planet->getIntelDate())}
							<ul class="garrison">
							{foreach $garrison as $id=>$nb}
							<li>{$nb} {$i18n->getText($enemy_ships[$id]->getName())}</li>
							{/foreach}
							</ul>
						{else}
							N/A
						{/if}
					{/if}
				</div>
			</li>
			{/foreach}
			</ul>
		{/if}
	{/if}
{/if}
<div style="clear:both;"></div>
<h1>{$lbl_political_system}</h1>
{$lbl_chief} : {if !is_null($chief)}{$chief->getName()}{else}N/A{/if}
{if isset($political_system)}
	{$system = $political_system}
	<p>{$i18n->getText($system->getName())}</p>
	<p>{$i18n->getText($system->getDefinition())}</p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_liberty}</span><img src="img/system.liberty.png"/> <progress value="{$system->getLiberty()}" max="100"></progress> {$system->getLiberty()}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_equality}</span><img src="img/system.equality.png"/> <progress value="{$system->getEquality()}" max="100"></progress> {$system->getEquality()}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_religion}</span><img src="img/system.religion.png"/> {$i18n->getText($system->getReligion())}</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_election}</span><img src="img/system.election.png"/> {if $system->getElection()}{$lbl_yes}{else}{$lbl_no}{/if}</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_production}</span><img src="img/system.production.png"/> <progress value="{$system->getProductionBonus() * 100}" max="100"></progress> {$system->getProductionBonus() * 100}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_moral}</span><img src="img/system.moral.png"/> <progress value="{$system->getMoralBonus() * 100}" max="100"></progress> {$system->getMoralBonus() * 100}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_attack}</span><img src="img/system.attack.png"/> <progress value="{$system->getAttackBonus() * 100}" max="100"></progress> {$system->getAttackBonus() * 100}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_defense}</span><img src="img/system.defense.png"/> <progress value="{$system->getDefenseBonus() * 100}" max="100"></progress> {$system->getDefenseBonus() * 100}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_corruption}</span><img src="img/system.corruption.png"/> <progress value="{$system->getCorruptionBonus() * 100}" max="100"></progress> {$system->getCorruptionBonus() * 100}%</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_justice}</span><img src="img/system.justice.png"/> {if $system->getIndependantJustice()}{$lbl_yes}{else}{$lbl_no}{/if}</a></p>
	<p><a class="system_item tooltip"><span class="classic">{$lbl_system_popcontrol}</span><img src="img/system.popcontrol.png"/> <progress value="{$system->getPopulationControl()}" max="100"></progress> {$system->getPopulationControl()}%</a></p>
{else}
	<ul id="political_systems">
	{foreach $political_systems as $system}
		<li>
			<p class="system_name">{$i18n->getText($system->getName())} (<a class="tooltip">?<span class="classic">{$i18n->getText($system->getDefinition())}</span></a>)</p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_liberty}</span><img src="img/system.liberty.png"/> <progress value="{$system->getLiberty()}" max="100"></progress> {$system->getLiberty()}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_equality}</span><img src="img/system.equality.png"/> <progress value="{$system->getEquality()}" max="100"></progress> {$system->getEquality()}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_religion}</span><img src="img/system.religion.png"/> {$i18n->getText($system->getReligion())}</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_election}</span><img src="img/system.election.png"/> {if $system->getElection()}{$lbl_yes}{else}{$lbl_no}{/if}</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_production}</span><img src="img/system.production.png"/> <progress value="{$system->getProductionBonus() * 100}" max="100"></progress> {$system->getProductionBonus() * 100}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_moral}</span><img src="img/system.moral.png"/> <progress value="{$system->getMoralBonus() * 100}" max="100"></progress> {$system->getMoralBonus() * 100}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_attack}</span><img src="img/system.attack.png"/> <progress value="{$system->getAttackBonus() * 100}" max="100"></progress> {$system->getAttackBonus() * 100}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_defense}</span><img src="img/system.defense.png"/> <progress value="{$system->getDefenseBonus() * 100}" max="100"></progress> {$system->getDefenseBonus() * 100}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_corruption}</span><img src="img/system.corruption.png"/> <progress value="{$system->getCorruptionBonus() * 100}" max="100"></progress> {$system->getCorruptionBonus() * 100}%</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_justice}</span><img src="img/system.justice.png"/> {if $system->getIndependantJustice()}{$lbl_yes}{else}{$lbl_no}{/if}</a></p>
			<p><a class="system_item tooltip"><span class="classic">{$lbl_system_popcontrol}</span><img src="img/system.popcontrol.png"/> <progress value="{$system->getPopulationControl()}" max="100"></progress> {$system->getPopulationControl()}%</a></p>
			<p class="system_choose"><form action="changepoliticalsystem.php" method="post">
				<input type="hidden" name="id" value="{$system->getId()}"/>
				<input type="hidden" name="token" value="{$token}"/>
				<input type="submit" value="{$lbl_choose}"/>
				</form>
			</p>
		</li>
	{/foreach}
	</ul>
{/if}
