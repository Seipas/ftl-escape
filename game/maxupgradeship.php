<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$shipid = $_GET['id'];
	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}
	$fleet = $player->getFleet();
	if (Helper::canRepair($fleet))
	{
		$ship = $fleet->getShip($shipid);

		if (is_null($ship))
		{
			echo "This ship does not seems to be in this fleet. Please go back.";
			exit;
		}

		$currentLevel = $ship->getLevel();
		if ($currentLevel < SHIP_UPGRADE_MAX_LEVEL)
		{
			$price = round($ship->getType()->getPrice() * SHIP_UPGRADE_PRICE * (SHIP_UPGRADE_MAX_LEVEL - $currentLevel) + $ship->getType()->getMaxHP($currentLevel) - $ship->getHP());
			if ($price > $fleet->getMaterial())
			{
				Tools::setFlashMsg($i18n->getText('msg.not.enough.material'));
			}
			else
			{
				$fleet->decreaseMaterial($price);
				$ship->setLevel(SHIP_UPGRADE_MAX_LEVEL);
				$ship->setHP($ship->getType()->getMaxHP(SHIP_UPGRADE_MAX_LEVEL));
				Tools::setFlashMsg($i18n->getText('msg.ship.upgraded',array($ship->getName(),SHIP_UPGRADE_MAX_LEVEL+1)));
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.ship.max.level'));
		}

		$entityManager->flush();
	}
	else
	{
		Tools::setFlashMsg($i18n->getText('msg.cannot.upgrade'));
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
