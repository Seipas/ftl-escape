<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');
require_once(__DIR__.'/../helper.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$fleet = $player->getFleet();

	$canRepair = false;
	$ships = $fleet->getShips();
	foreach ($ships as $s)
	{
		$canRepair = $canRepair || $s->getType()->canRepair();
	}

	if ($canRepair)
	{
		if (Helper::canAct($player) && $fleet->getMaterial() > 0)
		{
			$repairAmmount = 0;
			foreach ($ships as $ship)
			{
				$maxhp = $ship->getType()->getMaxHP($ship->getLevel());
				$repairAmmount += $maxhp - $ship->getHP();
			}
			if ($repairAmmount <= $fleet->getMaterial())
			{
				foreach ($ships as $ship)
				{
					$ammount = $ship->getType()->getMaxHP($ship->getLevel()) - $ship->getHP();
					$ship->setHP($ship->getType()->getMaxHP($ship->getLevel()));
					$ship->repairFTL();
					$ship->repairSurviveSystems();
					$fleet->decreaseMaterial($ammount);
				}
			}
			Tools::setFlashMsg($i18n->getText('msg.ships.repaired'));
		}
		elseif ($fleet->getMaterial() == 0)
		{
			Tools::setFlashMsg($i18n->getText('msg.not.enough.material'));
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.not.in.safe.space'));
		}
	}
	else
	{
		echo "This fleet has no repair ability.";
		exit;
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
