<?php

$event = $player->getEvent();
$smarty->assign('event_name',$i18n->getText($event->getName()));
$smarty->assign('event_description',$i18n->getText($event->getName().'.description'));
$smarty->assign('event_answers',$event->getAnswers());

// i18n
$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_solve',$i18n->getText('lbl.solve'));
$smarty->assign('lbl_event_question',$i18n->getText('lbl.event.question'));
$smarty->assign('lbl_event_result',$i18n->getText('lbl.event.result'));
$smarty->assign('lbl_event',$i18n->getText('lbl.event'));
$smarty->assign('lbl_nothing',$i18n->getText('lbl.nothing'));
$smarty->assign('lbl_moral',$i18n->getText('lbl.moral'));
$smarty->assign('lbl_fuel',$i18n->getText('lbl.fuel'));
$smarty->assign('lbl_food',$i18n->getText('lbl.food'));
$smarty->assign('lbl_material',$i18n->getText('lbl.material'));
$smarty->assign('lbl_medicine',$i18n->getText('lbl.medicine'));
$smarty->assign('lbl_staff',$i18n->getText('lbl.staff'));
$smarty->assign('lbl_passengers',$i18n->getText('lbl.passengers'));
$smarty->assign('lbl_nb_ships_damaged',$i18n->getText('lbl.nb.ships.damaged'));
$smarty->assign('lbl_damages',$i18n->getText('lbl.damages'));
$smarty->assign('lbl_possible_ennemies',$i18n->getText('lbl.possible.ennemies'));
$smarty->assign('lbl_possible_allies',$i18n->getText('lbl.possible.allies'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
