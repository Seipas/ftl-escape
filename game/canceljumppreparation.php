<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$fleet = $player->getFleet();

	$sector = $player->getSector();

	$ennemies = $sector->getEnnemies();

	if (count($ennemies) == 0)
	{
		$fleet->setJumpStatus(JUMP_STATUS_IDLE);
		Tools::setFlashMsg($i18n->getText('msg.jump.canceled'));
	}

	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
