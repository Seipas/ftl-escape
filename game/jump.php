<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$game = $entityManager->getRepository('Game')->find(1);
	if ($game->getStage() == STAGE_COUNTER_ATTACK_TWO)
	{
		Tools::setFlashMsg('msg.cannot.jump');
	}
	else
	{
		$username = $_SESSION['username'];
		$player = $entityManager->getRepository('Player')->findOneByLogin($username);

		$i18n = new I18n();
		$i18n->autoSetLang();
		if (Helper::hasEvent($player))
		{
			Tools::setFlashMsg($i18n->getText('msg.cannot.jump.while.event'));
		}
		else
		{
			Builder::jump($player);
			$entityManager->flush();
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
