<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    $fleet = $player->getFleet();
	$character = $entityManager->find('Character',$_GET['id']);
    if ($character->getFleet()->getId() == $fleet->getId())
    {
        $fleet->setFakedElection($character);
        $entityManager->flush();
        Tools::setFlashMsg($i18n->getText('msg.faked.election.for',array($character->getName())));
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=characters');
