<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_POST['token']))
{
    if (is_numeric($_POST['equipmentid']) && is_numeric($_POST['id']))
    {
        $planet = $entityManager->find('Planet',$_POST['id']);
        if (!is_null($planet->getFreedBy()) && $planet->getFreedBy()->getId() == $player->getId())
        {
            $equipmentType = $entityManager->find('PlanetaryEquipmentType',$_POST['equipmentid']);
            $fleet = $player->getFleet();
            if (count($planet->getEquipments()) < $planet->getMaxEquipments() && $equipmentType->getPrice() <= $fleet->getMaterial())
            {
                $build = true;
                if ($equipmentType->isUnique())
                {
                    $qb = $entityManager->createQueryBuilder();
                    $res = $qb->select('e')
                        ->from('PlanetaryEquipment','e')
                        ->where('e.planet = :planet')
                        ->andWhere('e.type = :type')
                        ->setParameter('planet',$planet)
                        ->setParameter('type',$equipmentType)
                        ->getQuery()
                        ->getResult();
                    $build = count($res) == 0;
                }
                if ($build)
                {
                    $fleet->decreaseMaterial($equipmentType->getPrice());
                    $equipment = new PlanetaryEquipment($planet,$equipmentType);
                    $entityManager->persist($equipment);
                }
                else
                {
                    Tools::setFlashMsg($i18n->getText('msg.cannot.build.equipment'));
                }
            }
            else
            {
                Tools::setFlashMsg($i18n->getText('msg.cannot.build.more.equipment'));
            }
        }
        $entityManager->flush();
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=planet&id='.$_POST['id']);
