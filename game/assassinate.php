<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    $fleet = $player->getFleet();
	if ($player->getAssassinationCount() < MAX_ASSASSINATION)
    {
        $character = $entityManager->find('Character',$_GET['id']);
        if ($character->getFleet()->getPlayer()->getId() == $player->getId())
        {
            if (is_null($fleet->getChief()) || $fleet->getChief()->getId() != $character->getId())
            {
                if (rand(1,100) <= ASSASSINATION_CHANCE)
                {
                    $popularity = $character->getPopularity();
                    // nothing personal...
                    if (rand(1,100) <= $fleet->getPoliticalSystem()->getPopulationControl())
                    {
                        $fleet->decreaseMoral(round($popularity/4));
                        Tools::setFlashMsg($i18n->getText('msg.assassination.full.success',array($character->getName())));
                    }
                    else
                    {
                        // i thought i had said to be discrete
                        $fleet->decreaseMoral($popularity);
                        $nbpopulation = rand(1,ASSASSINATION_MAX_POPULATION_KILLED);
                        $maxmaterial = $fleet->getMaterial() > ASSASSINATION_MAX_STOCK_DESTROYED ? ASSASSINATION_MAX_STOCK_DESTROYED : $fleet->getMaterial();
                        $nbmaterial = rand(1,$maxmaterial);
                        $maxfuel = $fleet->getFuel() > ASSASSINATION_MAX_STOCK_DESTROYED ? ASSASSINATION_MAX_STOCK_DESTROYED : $fleet->getFuel();
                        $nbfuel = rand(1,$maxfuel);
                        $maxfood = $fleet->getFood() > ASSASSINATION_MAX_STOCK_DESTROYED ? ASSASSINATION_MAX_STOCK_DESTROYED : $fleet->getFood();
                        $nbfood = rand(1,$maxfood);
                        Helper::killRandomPopulation($fleet,$nbpopulation);
                        $fleet->decreaseMaterial($nbmaterial);
                        $fleet->decreaseFuel($nbfuel);
                        $fleet->setFood($fleet->getFood()-$nbfood);
                        
                        Tools::setFlashMsg($i18n->getText('msg.assassination.half.success',array($character->getName(),$nbpopulation,$nbmaterial,$nbfuel,$nbfood)));
                    }
                    $entityManager->remove($character);
                }
                else
                {
                    Tools::setFlashMsg($i18n->getText('msg.assassination.failed',array($character->getName())));
                }
                $player->setAssassinationCount($player->getAssassinationCount() + 1);
            }
            else
            {
                Tools::setFlashMsg($i18n->getText('msg.cannot.commit.suicide'));
            }
        }
    }
    else
    {
        Tools::setFlashMsg($i18n->getText('msg.max.assassination'));
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
$entityManager->flush();
header('Location: index.php?page=characters');