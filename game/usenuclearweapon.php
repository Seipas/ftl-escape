<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_POST['token']))
{

	$fleet = $player->getFleet();
	$sector = $player->getSector();
    $nbweapons = $fleet->getNuclearWeapons();
    if ($nbweapons > 0)
    {
        if ($fleet->isEmpty())
        {
            Tools::gameOver($player);
            header('Location: index.php');
        }
        $player->setLastAttack(time());
        $ennemies = $sector->getEnnemies();
        $nbennemies = count($ennemies);
        $combinedattack = NUCLEAR_WEAPON_POWER;
        $averageattack = round(($combinedattack/$nbennemies),0);
        foreach ($ennemies as $ennemy)
        {
            $ennemy->takeDamage($averageattack);
            if ($ennemy->getHP() <= 0)
            {
                Tools::setFlashMsg($i18n->getText('msg.destroyed.ennemy.ship',array($ennemy->getName(),$i18n->getText($ennemy->getType()->getName()))), "green");
                $entityManager->remove($ennemy);
                $fleet->increaseNbEnemiesKilled();
                $sector->addWrecks(1);
            }
        }
        $entityManager->flush();
        
        // if the sector is a planet, we check if it is freed
        if (!is_null($sector->getPlanet()))
        {
            if (count($sector->getEnnemies()) == 0)
            {
                $planet = $sector->getPlanet();
                $planet->setStatus(0);
                $planet->setFreedBy($player);
            }
        }
		// same if it's a nexus...
		if (!is_null($sector->getNexus()))
		{
			if (count($sector->getEnnemies()) == 0)
            {
                $nexus = $sector->getNexus();
                $nexus->setStatus(4);
                $nexus->setDestroyedBy($player);
            }
		}
        $fleet->setNuclearWeapons($nbweapons - 1);
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
$entityManager->flush();
header('Location:index.php');
